/*
 * Copyright ©2002-2014 Skava.
 * All rights reserved.The Skava system, including
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and
 * other laws. Use without permission is prohibited.
 *
 * For further information contact Skava at info@skava.com.
 */
{
    "name": "aimia",
    "version": "v5",
    "campaignId": "1",
    "cookieManagerMap": {
        "aimiaCookieManager": {
            "skavaone.com": {
                "internalDomain": "skavaone.com",
                "externalDomain": "endeavourdemo.com",
                "externalDomainPattern": "endeavourdemo.com"
            }
        }
    },
    "globalParamMap": {
        "host_name": [
            "loyalty-mobile-connector-alpmobilesitv5.endeavourdemo.com"
        ],
        "host_name_checkout": [
            "identity-alpmobilesitv5.endeavourdemo.com"
        ],
		"host_name_appstage": [
            "aimia.skavaone.com"
        ],
        "key": [
            "Basic bWVtYmVyd2ViOm1lbWJlcndlYg=="
        ],
        "host_name_aimiacorecalls": [
            "aimia.skavaone.com/skavapim"
        ],
        "pimcampaignid": [
            "2295"
        ],
        "pricecall_user": [
            "aimia"
        ],
        "pricecall_pass": [
            "123"
        ],
        "host_name_evoucher":[
            "sandbox.woohoo.in"
        ],
        "poolcall_hostname":[
            "aimia.skavaone.com"
        ],
        "localhost_name":[
            "localhost"
        ],
        "streamstage_hostname":[
            "aimia.skavaone.com"
        ],
        "poolcall_campaign":[
            "9300"
        ],
        "auth_header":[
            "OAuth oauth_version=\"1.0\", oauth_signature_method=\"PLAINTEXT\", oauth_nonce=\"XRHklrylZttHLOz\", oauth_timestamp=\"1459770463\", oauth_consumer_key=\"7a770803117aeb7600ade5b6e65ccf66\", oauth_token=\"dbd419b63d3adf4ff6540d3bf4363ae3\", oauth_signature=\"a4f5e6b12abfd0fa909aa372480d1142%262c3ea4bf6728e39a802b3ea51f6631f3\""
        ]
    },
    "parse": {
        "types": {
            "RecommendationRecordaction": {
              "streamType": "true",
              "streamGroup": "marketing",
              "version": "v5",
              "partnerPattern": "aimia",
              "typeFamily": "recordAction",
              "idPattern": "null",
              "urlPattern": "null",
              "sources": {
                "RE_Record_Data": {
                  "type": "json",
                  "url": {
                    "url": {
                      "constant": "http://50.19.153.188/aham-scribe/aham/Scribe.json/Users/{@memberid}/activities?event={@action}&entity={@entity}&value={@productid}&sessionID={@sessionid}&orgid=AIMIA&accessChannel={@channel}&src={@src}"
                    },
                    "method": "GET"
                  },
                  "allowedResponseCodes": [
                    400,
                    404,
                    403,
                    401,
                    500,
                    412
                  ]
                }
              },
              "value": {
                "values": {
                  "type": {
                    "constant": "profile"
                  },
                  "properties": {
                    "type": "object",
                    "selector": ".//fault",
                    "source": "RE_Record_Data",
                    "object": {
                      "state": {
                        "type": "object_array",
                        "selector": ".",
                        "object": {
                          "status": {
                            "type": "string",
                            "selector": "//message[contains(.,'Server Exception')]",
                            "extractor": "(.+)",
                            "formatter": "Failed",
                            "formatterOnNull": "Success"
                          }
                        }
                      }
                    }
                  }
                }
              }
            },
            "ProductStrategyRecommendation_for_visualSimilarItems": {
              "streamType": "true",
              "streamGroup": "marketing",
              "version": "v5",
              "partnerPattern": "aimia",
              "typeFamily": "recommendation",
              "idPattern": "productStrategy",
              "urlPattern": "visualSimilarItems",
              "cacheKey": [
                {
                  "constant": "AIMIA_RECOMMENDATION_{@strategy}_{@productid}_{@memberid}"
                }
              ],
              "cacheInternalMapKey": [
                {
                  "constant": "response"
                }
              ],
              "cacheExpireData": {
                "overrideCachingTime": "3600000"
              },
              "sources": {
                "recom_product_list": {
                  "type": "jsonarray",
                  "url": {
                    "url": {
                      "constant": "http://50.19.153.188/aham-services-web/aham/Products.json/{@productid}/{@strategy}?mode=Light&limit={@limit}&orgid=AIMIA&channel={@channel}"
                    },
                    "method": "GET"
                  }
                }
              },
              "children": [
                {
                  "name": "products",
                  "selector": "/root/root",
                  "source": "recom_product_list",
                  "childDataName": "recom_products",
                  "params": {
                    "id": {
                      "type": "string",
                      "selector": "."
                    }
                  },
                  "typeSelector": {
                    "defaultType": "AIMIA_PRODUCT"
                  },
                  "addToMap": "children"
                }
              ]
            },
            "ProductStrategyRecommendation_for_globalTop": {
              "streamType": "true",
              "streamGroup": "marketing",
              "version": "v5",
              "partnerPattern": "aimia",
              "typeFamily": "recommendation",
              "idPattern": "productStrategy",
              "urlPattern": "globalTopSellingItems",
              "cacheKey": [
                {
                  "constant": "AIMIA_RECOMMENDATION_{@strategy}_{@memberid}"
                }
              ],
              "cacheInternalMapKey": [
                {
                  "constant": "response"
                }
              ],
              "cacheExpireData": {
                "overrideCachingTime": "3600000"
              },
              "sources": {
                "recom_product_list": {
                  "type": "jsonarray",
                  "url": {
                    "url": {
                      "constant": "http://50.19.153.188/aham-services-web/aham/Products.json/recommendations/{@strategy}?mode=Light&limit={@limit}&orgid=AIMIA&channel={@channel}"
                    },
                    "method": "GET"
                  }
                }
              },
              "children": [
                {
                  "name": "products",
                  "selector": "/root/root",
                  "source": "recom_product_list",
                  "childDataName": "recom_products",
                  "params": {
                    "id": {
                      "type": "string",
                      "selector": "."
                    }
                  },
                  "typeSelector": {
                    "defaultType": "AIMIA_PRODUCT"
                  },
                  "addToMap": "children"
                }
              ]
            },
            "ProductStrategyRecommendation_for_realTimeBoost": {
              "streamType": "true",
              "streamGroup": "marketing",
              "version": "v5",
              "partnerPattern": "aimia",
              "typeFamily": "recommendation",
              "idPattern": "productStrategy",
              "urlPattern": "getRealTimeBoostWrtProduct",
              "cacheKey": [
                {
                  "constant": "AIMIA_RECOMMENDATION_{@strategy}_{@productid}_{@memberid}"
                }
              ],
              "cacheInternalMapKey": [
                {
                  "constant": "response"
                }
              ],
              "cacheExpireData": {
                "overrideCachingTime": "3600000"
              },
              "sources": {
                "recom_product_list": {
                  "type": "jsonarray",
                  "url": {
                    "url": {
                      "constant": "http://50.19.153.188/aham-services-web/aham/Products.json/{@memberid}/{@productid}/{@filter}/{@strategy}?mode=Light&limit={@limit}&orgid=AIMIA&channel={@channel}"
                    },
                    "method": "GET"
                  }
                }
              },
              "children": [
                {
                  "name": "products",
                  "selector": "/root/root",
                  "source": "recom_product_list",
                  "childDataName": "recom_products",
                  "params": {
                    "id": {
                      "type": "string",
                      "selector": "."
                    }
                  },
                  "typeSelector": {
                    "defaultType": "AIMIA_PRODUCT"
                  },
                  "addToMap": "children"
                }
              ]
            },
            "ProductStrategyRecommendation": {
              "streamType": "true",
              "streamGroup": "marketing",
              "version": "v5",
              "partnerPattern": "aimia",
              "typeFamily": "recommendation",
              "idPattern": "productStrategy",
              "urlPattern": ".*",
              "cacheKey": [
                {
                  "constant": "AIMIA_RECOMMENDATION_{@strategy}_{@productid}_{@memberid}"
                }
              ],
              "cacheInternalMapKey": [
                {
                  "constant": "response"
                }
              ],
              "cacheExpireData": {
                "overrideCachingTime": "3600000"
              },
              "sources": {
                "recom_product_list": {
                  "type": "jsonarray",
                  "url": {
                    "url": {
                      "constant": "http://50.19.153.188/aham-services-web/aham/Products.json/{@productid}/recommendations/{@strategy}?mode=Light&limit={@limit}&orgid=AIMIA&channel={@channel}"
                    },
                    "method": "GET"
                  }
                }
              },
              "children": [
                {
                  "name": "products",
                  "selector": "/root/root",
                  "source": "recom_product_list",
                  "childDataName": "recom_products",
                  "params": {
                    "id": {
                      "type": "string",
                      "selector": "."
                    }
                  },
                  "typeSelector": {
                    "defaultType": "AIMIA_PRODUCT"
                  },
                  "addToMap": "children"
                }
              ]
            },
            "CategoryRecommendationById": {
              "streamType": "true",
              "streamGroup": "marketing",
              "version": "v5",
              "partnerPattern": "aimia",
              "typeFamily": "recommendation",
              "idPattern": "categoryStrategy",
              "urlPattern": "topsellers",
              "cacheKey": [
                {
                  "constant": "AIMIA_RECOMMENDATION_{@strategy}_{@memberid}"
                }
              ],
              "cacheInternalMapKey": [
                {
                  "constant": "response"
                }
              ],
              "cacheExpireData": {
                "overrideCachingTime": "3600000"
              },
              "sources": {
                "recom_product_list": {
                  "type": "jsonarray",
                  "url": {
                    "url": {
                      "constant": "http://50.19.153.188/aham-services-web/aham/Category.json/{@categoryid}/{@strategy}?mode=Light&limit={@limit}&orgid=AIMIA&channel={@channel}"
                    },
                    "method": "GET"
                  }
                }
              },
              "children": [
                {
                  "name": "products",
                  "selector": "/root/root",
                  "source": "recom_product_list",
                  "childDataName": "recom_products",
                  "params": {
                    "id": {
                      "type": "string",
                      "selector": "."
                    }
                  },
                  "typeSelector": {
                    "defaultType": "AIMIA_PRODUCT"
                  },
                  "addToMap": "children"
                }
              ]
            },
            "CategoryRecommendationByName": {
              "streamType": "true",
              "streamGroup": "marketing",
              "version": "v5",
              "partnerPattern": "aimia",
              "typeFamily": "recommendation",
              "idPattern": "categoryStrategy",
              "urlPattern": "topSellersByName",
              "cacheKey": [
                {
                  "constant": "AIMIA_RECOMMENDATION_{@strategy}_{@memberid}"
                }
              ],
              "cacheInternalMapKey": [
                {
                  "constant": "response"
                }
              ],
              "cacheExpireData": {
                "overrideCachingTime": "3600000"
              },
              "sources": {
                "recom_product_list": {
                  "type": "jsonarray",
                  "url": {
                    "url": {
                      "constant": "http://50.19.153.188/aham-services-web/aham/Category.json/{@category}/{@strategy}?mode=Light&limit={@limit}&orgid=AIMIA&channel={@channel}"
                    },
                    "method": "GET"
                  }
                }
              },
              "children": [
                {
                  "name": "products",
                  "selector": "/root/root",
                  "source": "recom_product_list",
                  "childDataName": "recom_products",
                  "params": {
                    "id": {
                      "type": "string",
                      "selector": "."
                    }
                  },
                  "typeSelector": {
                    "defaultType": "AIMIA_PRODUCT"
                  },
                  "addToMap": "children"
                }
              ]
            },
            "UserStrategyRecommendation": {
              "streamType": "true",
              "streamGroup": "marketing",
              "version": "v5",
              "partnerPattern": "aimia",
              "typeFamily": "recommendation",
              "idPattern": "userStrategy",
              "urlPattern": ".*",
              "cacheKey": [
                {
                  "constant": "AIMIA_RECOMMENDATION_{@strategy}_{@memberid}"
                }
              ],
              "cacheInternalMapKey": [
                {
                  "constant": "response"
                }
              ],
              "cacheExpireData": {
                "overrideCachingTime": "3600000"
              },
              "sources": {
                "recom_product_list": {
                  "type": "jsonarray",
                  "url": {
                    "url": {
                      "constant": "http://50.19.153.188/aham-services-web/aham/Users.json/{@memberid}/strategies/{@strategy}/recommendations?mode=Light&limit={@limit}&orgid=AIMIA&channel={@channel}&realTime=yes"
                    },
                    "method": "GET"
                  }
                }
              },
              "children": [
                {
                  "name": "products",
                  "selector": "/root/root",
                  "source": "recom_product_list",
                  "childDataName": "recom_products",
                  "params": {
                    "id": {
                      "type": "string",
                      "selector": "."
                    }
                  },
                  "typeSelector": {
                    "defaultType": "AIMIA_PRODUCT"
                  },
                  "addToMap": "children"
                }
              ]
            },
            "AIMIA_CATEGORY": {
                "streamType": "true",
                "streamGroup": "core",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "category",
                "idPattern": "null",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "cacheKey": [
                    {
                        "constant": "AIMIA_CATEGORY_{@pimcampaignid}_{@categoryid}"
                    },
                    {
                        "constant": "timestamp_{@timestamp}",
                        "paramNotRequired": "true"
                    },
                    {
                        "constant": "{@storeId}",
                        "paramNotRequired": "true"
                    },
					{
                        "constant": "{@locale}",
                        "paramNotRequired": "true"
                    }
                ],
                "cacheInternalMapKey": [
                    {
                        "constant": "data"
                    }
                ],
                "mustHaveCacheList": [
                    "children"
                ],
                "sources": {
                    "AIMIA_category_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "http://{@host_name_aimiacorecalls}/pim/category?campaignId={@pimcampaignid}"
                            },
                            "params": {
                                "categoryId": [
                                    {
                                        "constant": "{@categoryid}"
                                    }
                                ],
                                "locale": [
                                    {
                                        "constant": "{@locale}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "previewTime": [
                                    {
                                        "constant": "{@timestamp}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "storeId": [
                                    {
                                        "constant": "{@storeId}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ]
                            }
                        },
                        "fixJsonKeysForXML": true
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "category"
                        },
                        "responseCode": {
                            "type": "string",
                            "source": "AIMIA_category_data",
                            "selector": "//root/responseCode"
                        },
                        "responseMessage": {
                            "type": "string",
                            "source": "AIMIA_category_data",
                            "selector": "//root/responseMessage"
                        },
                        "name": {
                            "type": "string",
                            "source": "AIMIA_category_data",
                            "selector": "/root/category/name",
                            "extractor":"([^\\|]*)\\|*(.*)",
                            "formatter":"{@0}"
                        },
                        "link": {
                            "type": "string",
                            "source": "AIMIA_category_data",
                           "selector": "properties[./name='url']/value"
                        },
                        "identifier": {
                            "type": "string",
                            "source": "AIMIA_category_data",
                            "selector": "/root/category/categoryId|/root/category/categoryid"
                        },
                        "image":{
                            "type": "string",
                            "source": "AIMIA_category_data",
                            "selector": "/root/category/properties[./name='image']/value"
                        },
                        "navtype": {
                            "type": "string",
                            "source": "AIMIA_category_data",
                            "selector": "/root/category/categoryId|/root/category/categoryid",
                            "extractor":"(.*)",
                            "formatter": "identifier",
                            "formatterOnNull":""
                        },
                        "properties": {
                            "type": "object",
                            "source": "AIMIA_category_data",
                            "selector": "/root/category",
                            "object": {
                                "iteminfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "categoryids": {
                                            "type": "object_array",
                                            "selector": "parentCategoryId|parentcategoryid",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "local-name()"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": ".",
                                                    "extractor":".*\\|([^|]*)$",
                                                    "formatter":"{@0}",
                                                    "formatterOnNull":"{@0}"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "categories",
                        "source": "AIMIA_category_data",
                        "selector": "//root/category/categories",
                        "childDataName": "AIMIA_category_child_data",
                        "typeSelector": {
                            "defaultType": "AIMIA_CATEGORY_CHILD"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "AIMIA_CATEGORY_CHILD": {
                "value": {
                    "values": {
                        "type": {
                            "type": "string",
                            "source": "AIMIA_category_child_data",
                            "selector": "hasProducts|_hasProducts",
                            "extractor": "true",
                            "formatter": "productlist",
                            "formatterOnNull": "category"
                        },
                        "name": {
                            "type": "string",
                            "source": "AIMIA_category_child_data",
                            "selector": "name|_name",
                            "extractor":"([^\\|]*)\\|*(.*)",
                            "formatter":"{@0}"
                        },
                        "link": {
                            "type": "string",
                            "source": "AIMIA_category_child_data",
                            "selector": "properties[./name='url']/value"
                        },
                        "image": {
                            "type": "string",
                            "source": "AIMIA_category_child_data",
                            "selector": "properties[./name='image']/value"
                        },
                        "identifier": {
                            "type": "string",
                            "source": "AIMIA_category_child_data",
                            "selector": "categoryid|_categoryid|categoryId|_categoryId"
                        },
                        "navtype": {
                            "constant": "identifier"
                        },
                        "properties": {
                            "type": "object",
                            "selector": ".",
                            "source": "AIMIA_category_child_data",
                            "object": {
                                "iteminfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "categoryids": {
                                            "type": "object_array",
                                            "selector": "defaultParentCategoryId|parentCategoryId|_defaultParentCategoryId|_parentCategoryId|defaultparentcategoryid|parentcategoryid|_defaultparentcategoryid|_parentcategoryid",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "local-name()",
                                                    "extractor":"_(.*)",
                                                    "formatter":"{@0}",
                                                    "formatterOnNull":"{@0}"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": ".",
                                                    "extractor":".*\\|([^|]*)$",
                                                    "formatter":"{@0}",
                                                    "formatterOnNull":"{@0}"
                                                }
                                            }
                                        }
                                    }
                                },
                                 "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "productcount": {
                                            "type": "string",
                                            "selector": "_productCount"
                                        },
                                        "sorting": {
                                            "type": "object_array",
                                            "selector": "properties[./name[contains(text(),'sequence')]]",
                                            "object": {
                                                "options": {
                                                    "type": "object_array",
                                                    "selector": ".",
                                                    "object": {
                                                        "label": {
                                                            "type": "string",
                                                            "selector": "name"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "value"
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_PRODUCTLIST": {
                "streamType": "true",
                "streamGroup": "core",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "productlist",
                "idPattern": "null",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "cacheKey": [
                    {
                        "constant": "AIMIA_PRODUCTLIST_{@pimcampaignid}_{@categoryid}_{@offset}_{@limit}"
                    },
                    {
                        "constant": "timestamp_{@timestamp}",
                        "paramNotRequired": "true"
                    },
                    {
                        "constant": "{@filter}",
                        "paramNotRequired": "true"
                    },
                    {
                        "constant": "{@sort}",
                        "paramNotRequired": "true"
                    },
                    {
                        "constant": "{@storeId}",
                        "paramNotRequired": "true"
                    },
					{
                        "constant": "{@locale}",
                        "paramNotRequired": "true"
                    }
                ],
                "cacheInternalMapKey": [
                    {
                        "constant": "data"
                    }
                ],
                "mustHaveCacheList": [
                    "children"
                ],
                "sources": {
                    "AIMIA_productlist_data": {
                        "type": "json",
                        "url": {
                            "url": {
                            "constant": "http://{@host_name_aimiacorecalls}/pim/productlist?campaignId={@pimcampaignid}"
                            },
                            "params": {
                                "categoryId": [
                                    {
                                        "constant": "{@categoryid}"
                                    }
                                ],
                                "filter": [
                                    {
                                        "constant": "{@filter}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "sort": [
                                    {
                                        "constant": "{@sort}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "offset": [
                                    {
                                        "constant": "{@offset}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "limit": [
                                    {
                                        "constant": "{@limit}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "locale": [
                                    {
                                        "constant": "{@locale}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "previewTime": [
                                    {
                                        "constant": "{@timestamp}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "storeId": [
                                    {
                                        "constant": "{@storeId}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ]
                            }
                        },
                        "fixJsonKeysForXML": true,
                        "appendToJSONKey": "_"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "productlist"
                        },
                        "responseCode": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "/root/_responseCode|/root/responseCode"
                        },
                        "responseMessage": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "/root/_responseMessage|/root/responseMessage"
                        },
                        "name": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "/root/_category/_name|/root/category/name",
                            "extractor":"([^\\|]*)\\|*(.*)",
                            "formatter":"{@0}"
                        },
                        "link": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "//root/_category/_properties[./_name[text()='Category_Url']]/_value"
                        },
                        "image": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "//root/_category/_properties[./_name[text()='image']]/_value"
                        },
                        "identifier": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "//root/_category/_categoryid|//root/category/categoryid|//root/_category/_categoryId|//root/category/categoryId"
                        },  
                        "facets":{
                            "type":"string",
                            "source": "AIMIA_productlist_data",
                            "selector":"//root/_facets",
                            "txtToJson":"true"
                        },                        
                        "properties": {
                            "type": "object",
                            "selector": "/root",
                            "source": "AIMIA_productlist_data",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "productcount": {
                                            "type": "string",
                                            "selector": "_productCount|productCount"
                                        },
                                        "count": {
                                            "type": "string",
                                            "selector": "_subcategorycount|subcategorycount"
                                        },
                                        "sorting": {
                                            "type": "object_array",
                                            "selector": ".",
                                            "object": {
                                                "selectedname": {
                                                    "type": "string",
                                                    "selector": "_selectedSort/_name"
                                                },
                                                "selectedidentifier":{
                                                    "type": "string",
                                                    "selector": "_selectedSort/_value"
                                                },
                                                "options": {
                                                    "type": "object_array",
                                                    "selector": "_sorts|sorts",
                                                    "object": {
                                                        "label": {
                                                            "type": "string",
                                                            "selector": "_name|name|label|_label"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "_value|value"
                                                        }
                                                    }
                                                }
                                            }
                                        },
										"selectedFacets": {
                                            "type": "object_array",
                                            "selector": "_selectedFacets|selectedFacets",
                                            "object": {
												"label": {
													"type": "string",
													"selector": "_key|key"
												},
												"value": {
													"type": "string",
													"selector": "_value|value"
												}
                                            }
                                        }
                                    }
                                },
								
                                "iteminfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "categoryids": {
                                            "type": "object_array",
                                            "source": "AIMIA_productlist_data",
                                            "selector": "/root/_category/_parentCategoryId|/root/category/parentCategoryId|/root/_category/_parentCategoryid|/root/category/parentCategoryid",
                                            "object": {
                                                "label": {
                                                    "constant": "parentcategoryid"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": ".",
                                                    "extractor":".*\\|([^|]*)$",
                                                    "formatter":"{@0}",
                                                    "formatterOnNull":"{@0}"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "products",
                        "source": "AIMIA_productlist_data",
                        "selector": "//root/_products|//root/products",
                        "childDataName": "AIMIA_productlist_child_data",
                        "typeSelector": {
                            "defaultType": "AIMIA_PRODUCTLIST_CHILD"
                        },
                        "addToMap": "children"
                    },
                    {
                        "name": "categories",
                        "source": "AIMIA_productlist_data",
                        "selector": "//root/_category|//root/category",
                        "childDataName": "AIMIA_category_child_data",
                        "typeSelector": {
                            "defaultType": "AIMIA_CATEGORY_CHILD_PRODUCTLIST"
                        },
                        "addToMap": "children"
                    }
                ]
            },
			"AIMIA_PRODUCTLIST_M": {
                "streamType": "true",
                "streamGroup": "core",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "productlist",
                "idPattern": ".*",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "cacheKey": [
                    {
                        "constant": "AIMIA_PRODUCTLIST_M_{@pimcampaignid}_{@categoryid}_{@offset}_{@limit}"
                    },
                    {
                        "constant": "timestamp_{@timestamp}",
                        "paramNotRequired": "true"
                    },
                    {
                        "constant": "{@filter}",
                        "paramNotRequired": "true"
                    },
                    {
                        "constant": "{@sort}",
                        "paramNotRequired": "true"
                    },
                    {
                        "constant": "{@storeId}",
                        "paramNotRequired": "true"
                    },
					{
                        "constant": "{@locale}",
                        "paramNotRequired": "true"
                    }
                ],
                "cacheInternalMapKey": [
                    {
                        "constant": "data"
                    }
                ],
                "mustHaveCacheList": [
                    "children"
                ],
                "sources": {
                    "AIMIA_productlist_data": {
                        "type": "json",
                        "url": {
                            "url": {
							"constant": "http://{@host_name_appstage}/skavapim/pim/productlist1?campaignId={@pimcampaignid}"
                            },
                            "params": {
                                "categoryId": [
                                    {
                                        "constant": "{@categoryid}"
                                    }
                                ],
                                "filter": [
                                    {
                                        "constant": "{@filter}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "sort": [
                                    {
                                        "constant": "{@sort}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "offset": [
                                    {
                                        "constant": "{@offset}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "limit": [
                                    {
                                        "constant": "{@limit}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "locale": [
                                    {
                                        "constant": "{@locale}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "previewTime": [
                                    {
                                        "constant": "{@timestamp}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "storeId": [
                                    {
                                        "constant": "{@storeId}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ]
                            }
                        },
                        "fixJsonKeysForXML": true,
                        "appendToJSONKey": "_"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "productlist"
                        },
                        "responseCode": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "/root/_responseCode|/root/responseCode"
                        },
                        "responseMessage": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "/root/_responseMessage|/root/responseMessage"
                        },
                        "name": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "/root/_category/_name|/root/category/name",
                            "extractor":"([^\\|]*)\\|*(.*)",
                            "formatter":"{@0}"
                        },
                        "link": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "//root/_category/_properties[./_name[text()='Category_Url']]/_value"
                        },
                        "image": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "//root/_category/_properties[./_name[text()='image']]/_value"
                        },
                        "identifier": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "//root/_category/_categoryid|//root/category/categoryid|//root/_category/_categoryId|//root/category/categoryId"
                        },  
                        "facets":{
                            "type":"string",
                            "source": "AIMIA_productlist_data",
                            "selector":"//root/_facets",
                            "txtToJson":"true"
                        },                        
                        "properties": {
                            "type": "object",
                            "selector": "/root",
                            "source": "AIMIA_productlist_data",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "productcount": {
                                            "type": "string",
                                            "selector": "_productCount|productCount"
                                        },
                                        "count": {
                                            "type": "string",
                                            "selector": "_subcategorycount|subcategorycount"
                                        },
                                        "sorting": {
                                            "type": "object_array",
                                            "selector": ".",
                                            "object": {
                                                "selectedname": {
                                                    "type": "string",
                                                    "selector": "_selectedSort/_name"
                                                },
                                                "selectedidentifier":{
                                                    "type": "string",
                                                    "selector": "_selectedSort/_value"
                                                },
                                                "options": {
                                                    "type": "object_array",
                                                    "selector": "_sorts|sorts",
                                                    "object": {
                                                        "label": {
                                                            "type": "string",
                                                            "selector": "_name|name|label|_label"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "_value|value"
                                                        }
                                                    }
                                                }
                                            }
                                        },
										"selectedFacets": {
                                            "type": "object_array",
                                            "selector": "_selectedFacets|selectedFacets",
                                            "object": {
												"label": {
													"type": "string",
													"selector": "_key|key"
												},
												"value": {
													"type": "string",
													"selector": "_value|value"
												}
                                            }
                                        }
                                    }
                                },								
                                "iteminfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "categoryids": {
                                            "type": "object_array",
                                            "source": "AIMIA_productlist_data",
                                            "selector": "/root/_category/_parentCategoryId|/root/category/parentCategoryId|/root/_category/_parentCategoryid|/root/category/parentCategoryid",
                                            "object": {
                                                "label": {
                                                    "constant": "parentcategoryid"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": ".",
                                                    "extractor":".*\\|([^|]*)$",
                                                    "formatter":"{@0}",
                                                    "formatterOnNull":"{@0}"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "products",
                        "source": "AIMIA_productlist_data",
                        "selector": "//root/_products|//root/products",
                        "childDataName": "AIMIA_productlist_child_data",
                        "typeSelector": {
                            "defaultType": "AIMIA_PRODUCTLIST_CHILD"
                        },
                        "addToMap": "children"
                    },
                    {
                        "name": "categories",
                        "source": "AIMIA_productlist_data",
                        "selector": "//root/_category|//root/category",
                        "childDataName": "AIMIA_category_child_data",
                        "typeSelector": {
                            "defaultType": "AIMIA_CATEGORY_CHILD_PRODUCTLIST"
                        },
                        "addToMap": "children"
                    }
                ]
            },
			"AIMIA_PRODUCTLIST_SEARCH": {
                "streamType": "true",
                "streamGroup": "core",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "search",
                "idPattern": ".*",
                "urlPattern": "null",
                "sources": {
                    "AIMIA_productlist_data": {
                        "type": "json",
                        "url": {
                            "url": {
							"constant": "http://{@host_name_appstage}/skavapim/pim/getproducts?campaignId={@pimcampaignid}"
                            },
                            "params": {                            
							   "category": [
                                    {
                                        "constant": "{@category}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
								 "search": [
                                    {
                                        "constant": "{@search}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "selectedFacets": [
                                    {
                                        "constant": "{@selectedFacets}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
								"selectedFacetsv2": [
                                    {
                                        "constant": "{@selectedFacetsv2}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "group": [
                                    {
                                        "constant": "{@group}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "sort": [
                                    {
                                        "constant": "{@sort}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "offset": [
                                    {
                                        "constant": "{@offset}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "limit": [
                                    {
                                        "constant": "{@limit}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "locale": [
                                    {
                                        "constant": "{@locale}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "previewTime": [
                                    {
                                        "constant": "{@timestamp}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "storeId": [
                                    {
                                        "constant": "{@storeId}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ]
                            }
                        },
                        "fixJsonKeysForXML": true,
                        "appendToJSONKey": "_"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "productlist"
                        },
                        "responseCode": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "/root/_responseCode|/root/responseCode"
                        },
                        "responseMessage": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "/root/_responseMessage|/root/responseMessage"
                        },
                        "name": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "/root/_category/_name|/root/category/name",
                            "extractor":"([^\\|]*)\\|*(.*)",
                            "formatter":"{@0}"
                        },
                        "link": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "//root/_category/_properties[./_name[text()='Category_Url']]/_value"
                        },
                        "image": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "//root/_category/_properties[./_name[text()='image']]/_value"
                        },
                         "identifier": {
                            "type": "string",
                            "source": "AIMIA_productlist_data",
                            "selector": "//root/_category/_categoryid|//root/category/categoryid|//root/_category/_categoryId|//root/category/categoryId"
                        }, 
                        "facets":{
                            "type":"string",
                            "source": "AIMIA_productlist_data",
                            "selector":"//root/_facets",
                            "txtToJson":"true"
                        },                        
                        "properties": {
                            "type": "object",
                            "selector": "/root",
                            "source": "AIMIA_productlist_data",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "productcount": {
                                            "type": "string",
                                            "selector": "_productCount|productCount"
                                        },
                                        "count": {
                                            "type": "string",
                                            "selector": "_subcategorycount|subcategorycount"
                                        },
                                        "sorting": {
                                            "type": "object_array",
                                            "selector": ".",
                                            "object": {
                                                "selectedname": {
                                                    "type": "string",
                                                    "selector": "_selectedSort/_name"
                                                },
                                                "selectedidentifier":{
                                                    "type": "string",
                                                    "selector": "_selectedSort/_value"
                                                },
                                                "options": {
                                                    "type": "object_array",
                                                    "selector": "_sorts|sorts",
                                                    "object": {
                                                        "label": {
                                                            "type": "string",
                                                            "selector": "_name|name|label|_label"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "_value|value"
                                                        }
                                                    }
                                                }
                                            }
                                        },
										"selectedFacets": {
                                            "type": "object_array",
                                            "selector": "_selectedFacets|selectedFacets",
                                            "object": {
												"label": {
													"type": "string",
													"selector": "_key|key"
												},
												"value": {
													"type": "string",
													"selector": "_value|value"
												}
                                            }
                                        }
                                    }
                                },								
                                "iteminfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "categoryids": {
                                            "type": "object_array",
                                            "source": "AIMIA_productlist_data",
                                            "selector": "/root/_category/_parentCategoryId|/root/category/parentCategoryId|/root/_category/_parentCategoryid|/root/category/parentCategoryid",
                                            "object": {
                                                "label": {
                                                    "constant": "parentcategoryid"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": ".",
                                                    "extractor":".*\\|([^|]*)$",
                                                    "formatter":"{@0}",
                                                    "formatterOnNull":"{@0}"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "products",
                        "source": "AIMIA_productlist_data",
                        "selector": "//root/_products|//root/products",
                        "childDataName": "AIMIA_productlist_child_data",
                        "typeSelector": {
                            "defaultType": "AIMIA_PRODUCTLIST_CHILD"
                        },
                        "addToMap": "children"
                    },
                    {
                        "name": "categories",
                        "source": "AIMIA_productlist_data",
                        "selector": "//root/_category|//root/category",
                        "childDataName": "AIMIA_category_child_data",
                        "typeSelector": {
                            "defaultType": "AIMIA_CATEGORY_CHILD_PRODUCTLIST"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "AIMIA_CATEGORY_CHILD_PRODUCTLIST":{
                "value": {
                    "values": {
                        "type": {
                            "type": "string",
                            "source": "AIMIA_category_child_data",
                            "selector": "hasProducts|_hasProducts",
                            "extractor": "true",
                            "formatter": "productlist",
                            "formatterOnNull": "category"
                        },
                        "name": {
                            "type": "string",
                            "source": "AIMIA_category_child_data",
                            "selector": "name|_name",
                            "extractor":"([^\\|]*)\\|*(.*)",
                            "formatter":"{@0}"
                        },
                        "link": {
                            "type": "string",
                            "source": "AIMIA_category_child_data",
                            "selector": "properties[./name='url']/value"
                        },
                        "image": {
                            "type": "string",
                            "source": "AIMIA_category_child_data",
                            "selector": "properties[./name='image']/value|_properties[./_name='image']/_value"
                        },
                        "identifier": {
                            "type": "string",
                            "source": "AIMIA_category_child_data",
                            "selector": "categoryid|_categoryid|categoryId|_categoryId"
                        },
                        "navtype": {
                            "constant": "identifier"
                        },
                        "properties": {
                            "type": "object",
                            "selector": ".",
                            "source": "AIMIA_category_child_data",
                            "object": {
                                 "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "productcount": {
                                            "type": "string",
                                            "selector": "_productCount"
                                        }
                                    }
                                },
                                "iteminfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "categoryids": {
                                            "type": "object_array",
                                            "selector": "_parentcategoryid|_parentCategoryId",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "local-name()",
                                                    "extractor": "_(.*)",
                                                    "formatter": "{@0}"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": ".",
                                                    "extractor":".*\\|([^|]*)$",
                                                    "formatter":"{@0}",
                                                    "formatterOnNull":"{@0}"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "categories",
                        "source": "AIMIA_category_child_data",
                        "selector": "_categories",
                        "childDataName": "AIMIA_category_child_data",
                        "typeSelector": {
                            "defaultType": "AIMIA_CATEGORY_CHILD_PRODUCTLIST"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "AIMIA_PRODUCTLIST_CHILD": {
                "value": {
                    "values": {
                        "type": {
                            "constant": "product"
                        },
                        "name": {
                            "type": "string",
                            "source": "AIMIA_productlist_child_data",
                            "selector": "_name|name"
                        },
                        "image": {
                            "type": "string",
                            "source": "AIMIA_productlist_child_data",
                            "selector": "_properties[./_name='image']/_value|properties[./name='image']/value"
                        },
                        "link": {
                            "type": "string",
                            "source": "AIMIA_productlist_child_data",
                            "selector": "_skus/_properties[./_name='skuurl']/_value|skus/properties[./name='skuurl']/value|_properties[./_name='skuurl']/_value"
                        },
                        "identifier": {
                            "type": "string",
                            "source": "AIMIA_productlist_child_data",
                            "multiSelector": [
                                "_skuid|skuid|_skuId|skuId",
                                "_productId|productId|_productid|productid"
                            ]
                        },
                        "navtype": {
                            "constant": "identifier"
                        },
                        "properties": {
                            "type": "object",
                            "selector": ".",
                            "source": "AIMIA_productlist_child_data",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                       "displayorder": {
                                            "type": "string",
                                            "selector": "_properties[./_name[text()='rank']]/_value"
                                        }
                                    }
                                },
                                "cartinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "expecteddelivery": {
                                            "type": "string",
                                            "selector": "_properties[./_name[text()='deliverydaysmin']]/_value"
                                        },
                                        "expecteddeliverydays": {
                                            "type": "string",
                                            "selector": "_properties[./_name[text()='deliverydaysmax']]/_value"
                                        }
                                    }
                                },
                                "skuprops": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "color": {
                                            "type": "object_array",
                                            "selector": "_properties[./_name='color']/_value|properties[./name='color']/value|_properties[./_name='colors']/_value",
                                            "object": {
                                                "name": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "size1": {
                                            "type": "object_array",
                                            "selector": "_properties[./_name='size1']/_value|properties[./name='size1']/value",
                                            "object": {
                                                "name": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                },
                                "iteminfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "categoryids": {
                                            "type": "object_array",
                                            "source": "AIMIA_productlist_child_data",
                                            "selector": "_defaultParentCategoryId|defaultParentCategoryId|_defaultParentCategoryid|defaultParentCategoryid",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "local-name()",
                                                    "extractor":"_(.*)",
                                                    "formatter":"{@0}",
                                                    "formatterOnNull":"{@0}"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": ".",
                                                    "extractor":".*\\|([^|]*)$",
                                                    "formatter":"{@0}",
                                                    "formatterOnNull":"{@0}"
                                                }
                                            }
                                        },
                                        "bigimage": {
                                            "type": "object_array",
                                            "source": "AIMIA_productlist_child_data",
                                            "selector": "_properties[./_name='zoom']/_value|properties[./name='zoom']/value",
                                            "object": {
                                                "image": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "thumbimage": {
                                            "type": "object_array",
                                            "source": "AIMIA_productlist_child_data",
                                            "selector": "_properties[./_name='thumb']/_value|properties[./name='thumb']/value",
                                            "object": {
                                                "image": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "brand": {
                                            "type": "string",
                                            "selector": "_properties[./_name='brand']/_value|properties[./name='brand']/value"
                                        },
                                        "iscollection": {
                                            "type": "string",
                                            "selector": "_collection|collection"
                                        },
                                        "additionalinfo": {
                                            "type": "object_array",
                                            "selector": "_properties[./_name[text()='sellerName']]/_value",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "./../_name"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "additionalimages":{
                                            "type":"string",
                                            "selector":"_properties[./_name='altimages']/_value",
                                            "txtToJson":"true"
                                        }
                                    },
                                    "serialObject": [
                                        {
                                            "name": "parent",
                                            "type": "object",
                                            "source": "AIMIA_productlist_child_data",
                                            "selector": "_defaultParentCategoryId|defaultParentCategoryId|_defaultParentCategoryid|defaultParentCategoryid",
                                            "object": {
                                                "label": {
                                                    "constant":"parentcategoryid"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            },
                                            "addToArray": "categoryids"
                                        }
                                    ]
                                },
                                "buyinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "isnew":{
                                            "type": "string",
                                            "selector": "_properties[./_name='new']/_value|properties[./name='new']/value"
                                        },
                                        "instock": {
                                            "type": "string",
                                            "selector": "_active|active"
                                        },
                                        "rewards":{
                                            "type":"object_array",
                                            "selector":"_properties[./_name='loyaltyPoints']/_value",
                                            "object":{
                                                "points":{
                                                    "type":"string",
                                                    "selector":"."
                                                }
                                            }   
                                        },
                                        "pricing": {
                                            "type": "object",
                                            "selector": ".",
                                            "object":{
                                                "currencycode":{
                                                    "constant":"INR"
                                                }
                                            },
                                            "serialObject": [
                                                {
                                                    "name": "price",
                                                    "type": "object",
                                                    "selector": "_properties[./_name='mrp']/_value|properties[./name='mrp']/value",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Reg Price"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "type": {
                                                            "constant": "Reg"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "price",
                                                    "type": "object",
                                                    "selector": "_properties[./_name='regularPrice']/_value|properties[./name='regularPrice']/value",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Reg Price"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "type": {
                                                            "constant": "Reg"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "price",
                                                    "type": "object",
                                                    "selector": "_properties[./_name='price']/_value|properties[./name='price']/value",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Sale Price"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "type": {
                                                            "constant": "Sale"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                }
                                            ]
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object_array",
                                    "selector": "_properties[./_name='gender']/_value|properties[./name='gender']/value",
                                    "object": {
                                        "gender": {
                                            "type": "string",
                                            "selector": "."
                                        }
                                    }
                                },
                                "reviewrating": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "rating": {
                                            "type": "string",
                                            "selector": "_properties[./_name='aggregated_rating']/_value|properties[./name='aggregated_rating']/value"
                                        },
                                        "reviewcount": {
                                            "type": "string",
                                            "selector": "_properties[./_name='reviewcount']/_value|properties[./name='reviewcount']/value|_properties[./_name='review_count']/_value"
                                        },
                                        "ratings": {
                                            "type": "object_array",
                                            "selector": "_properties[./_name='max_rating']|_properties[./_name='aggregated_rating']|properties[./name='max_rating']|properties[./name='aggregated_rating']|_properties[./_name='avg_rating']",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "_name"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": "_value"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "skus",
                        "source": "AIMIA_productlist_child_data",
                        "selector": "_skus|skus",
                        "childDataName": "AIMIA_productlist_child_data",
                        "typeSelector": {
                            "defaultType": "AIMIA_PRODUCTLIST_CHILD"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "AIMIA_PRODUCT": {
                "streamType": "true",
                "streamGroup": "core",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "product",
                "idPattern": ".*",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "cacheKey": [
                    {
                        "constant": "AIMIA_PRODUCT_{@pimcampaignid}_{@id}"
                    },
                    {
                        "constant": "timestamp_{@timestamp}",
                        "paramNotRequired": "true"
                    },
                    {
                        "constant": "{@storeId}",
                        "paramNotRequired": "true"
                    },
					{
                        "constant": "{@locale}",
                        "paramNotRequired": "true"
                    }
                ],
                "cacheInternalMapKey": [
                    {
                        "constant": "data"
                    }
                ],
                "mustHaveCacheList": [
                    "children"
                ],
                "sources": {
                    "AIMIA_product_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "http://{@host_name_aimiacorecalls}/pim/product?campaignId={@pimcampaignid}&productId={@id}"
                            },
                            "params": {
                                "locale": [
                                    {
                                        "constant": "{@locale}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "previewTime": [
                                    {
                                        "constant": "{@timestamp}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "storeId": [
                                    {
                                        "constant": "{@storeId}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ]
                            },
                            "additionalParses":[
                                {
                                    "type":"jsonarray",
                                    "name":"aimia_images_data",
                                    "startStr":"\"properties\":{\"altimages\":\"",
                                    "endStr":"]",
                                    "includeStart": false,
                                    "includeEnd": true,
                                    "doUnescapeJavaString": "true"
                            }
                            ]
                        },
                        "fixJsonKeysForXML": true
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "product"
                        },
                        "responseCode": {
                            "type": "string",
                            "source": "AIMIA_product_data",
                            "selector": "//root/responseCode"
                        },
                        "responseMessage": {
                            "type": "string",
                            "source": "AIMIA_product_data",
                            "selector": "//root/responseMessage"
                        },
                        "name": {
                            "type": "string",
                            "source": "AIMIA_product_data",
                            "selector": "/root/product/name"
                        },
                        "image": {
                            "type": "string",
                            "source": "AIMIA_product_data",
                            "selector": "/root/product/properties[./name='image']/value"
                        },
                        "link": {
                            "type": "string",
                            "source": "AIMIA_product_data",
                            "selector": "/root/product/skus/properties[./name='skuurl']/value|/root/product/properties[./name='skuurl']/value",
                            "extractor": "(.*)\\?(.*)$",
                            "formatter": "{@0}"
                        },
                        "identifier": {
                            "type": "string",
                            "source": "AIMIA_product_data",
                            "multiSelector": [
                                "/root/product/skuId|/root/product/skuid",
                                "/root/product/productId|/root/product/productid"
                            ]
                        },
                        "properties": {
                            "type": "object",
                            "selector": "/root/product",
                            "source": "AIMIA_product_data",
                            "params": {
                                "orgregprice": {
                                    "type": "string",
                                    "selector": "./properties[./name='mrp']/value",
                                    "skipFromMustCheck": true
                                },
                                "orgsaleprice": {
                                    "type": "string",
                                    "selector": "./properties[./name='price']/value",
                                    "skipFromMustCheck": true
                                }
                            },
                            "object": {
                                "state":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "displayorder": {
                                            "type": "string",
                                            "selector": "properties[./name[text()='rank']]/value"
                                        }
                                    }
                                },
                                "skuprops": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "color": {
                                            "type": "object_array",
                                            "selector": "properties[./name='color']/value|properties[./name='colors']/value",
                                            "object": {
                                                "name": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "size": {
                                            "type": "object_array",
                                            "selector": "properties[./name='size']/value",
                                            "object": {
                                                "name": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                },
                                "iteminfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "categoryids": {
                                            "type": "object_array",
                                            "source": "AIMIA_product_data",
                                            "selector": "/root/product/defaultParentCategoryId|/root/product/defaultParentCategoryid",
                                            "object": {
                                                "label": {
                                                    "constant": "parent category"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": ".",
                                                    "extractor":".*\\|([^|]*)$",
                                                    "formatter":"{@0}"
                                                }
                                            }
                                        },
                                        "brand": {
                                            "type": "string",
                                            "selector": "properties[./name='brand']/value"
                                        },
                                        "iscollection": {
                                            "type": "string",
                                            "selector": "collection"
                                        },
                                        "bigimage": {
                                            "type": "object_array",
                                            "source": "AIMIA_product_data",
                                            "selector": "/root/product/properties[./name='zoom']/value",
                                            "object": {
                                                "image": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "thumbimage": {
                                            "type": "object_array",
                                            "source": "AIMIA_product_data",
                                            "selector": "/root/product/properties[./name='thumb']/value",
                                            "object": {
                                                "image": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "description": {
                                            "type": "object_array",
                                            "selector": "properties[./name='description']/value",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "info"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "specifications": {
                                            "type": "string",
                                            "selector": "/root/product/properties[./name='tag']/value|/root/product/properties[./name='tags']/value",
                                            "txtToJson": "true"
                                        },
                                        "additionalimages": {
                                            "type":"string",
                                            "selector":"properties[./name='altimages']/value",
                                            "txtToJson": "true"
                                        },
                                        "additionalinfo": {
                                            "type": "object_array",
                                            "selector": "./properties[./name[text()='sellerName']]/value",
                                            "object":{
                                                "label": {
                                                    "type":"string",
                                                    "selector": "./../name"
                                                },
                                                "value":{
                                                    "type":"string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    },
                                    "serialObject": [
                                        {
                                            "name": "parent",
                                            "type": "object",
                                            "source": "AIMIA_product_data",
                                            "selector": "/root/product/defaultParentCategoryId|/root/product/defaultParentCategoryid",
                                            "object": {
                                                "label": {
                                                    "constant": "parentcategoryid"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            },
                                            "addToArray": "categoryids"
                                        }
                                    ]
                                },
                                "buyinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "sources": {
                                        "AIMIA_price_data": {
                                            "type": "forcedjsonarray",
                                            "url": {
                                                "url": {
                                                    "constant": "http://api.intelligencenode.com/real_time_price"
                                                },
                                                "method": "POST",
                                                "contentType": "application/json",
                                                "content": "{\"ids\":[\"{@id}\"],\"user\":\"{@pricecall_user}\",\"pass\":\"{@pricecall_pass}\"}"
                                            },
                                            "allowedResponseCodes": [
                                                400
                                            ]
                                        }
                                    },
                                    "object": {
                                        "isnew":{
                                            "type": "string",
                                            "selector": "/root/product/properties[./name='new']/value"
                                        },
                                        "rewards":{
                                            "type":"object_array",
                                            "selector":".",
                                            "object":{
                                                "points":{
                                                    "type":"string",
                                                    "selector":"string(floor(/root/product/properties[./name='loyaltyPoints']/value))",
                                                    "extractor":"NaN",
                                                    "formatter":"0",
                                                    "formatterOnNull":"{@0}"
                                                }
                                            }   
                                        },
                                        "pricing": {
                                            "type": "object",
                                            "selector": ".",
                                            "params": {
                                                "orgregprice": {
                                                    "type": "string",
                                                    "selector": "/root/root/mrp",
                                                    "source": "AIMIA_price_data",
                                                    "skipFromMustCheck": true,
                                                    "paramNotRequired": "true",
                                                    "emptyCheck": "true"
                                                },
                                                "orgsaleprice": {
                                                    "type": "string",
                                                    "selector": "/root/root/price",
                                                    "source": "AIMIA_price_data",
                                                    "skipFromMustCheck": true,
                                                    "paramNotRequired": "true",
                                                    "emptyCheck": "true"
                                                }
                                            },
                                            "object":{
                                                "currencycode":{
                                                    "constant":"INR"
                                                }
                                            },
                                            "serialObject": [
                                                {
                                                    "name": "price",
                                                    "source": "AIMIA_price_data",
                                                    "selector": ".[{@orgregprice}!='null']",
                                                    "type": "object",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Reg Price"
                                                        },
                                                        "value": {
                                                            "constant": "{@orgregprice}"
                                                        },
                                                        "type": {
                                                            "constant": "Reg"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "price",
                                                    "source": "AIMIA_price_data",
                                                    "selector": ".[{@orgsaleprice}!='null']",
                                                    "type": "object",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Sale Price"
                                                        },
                                                        "value": {
                                                            "constant": "{@orgsaleprice}"
                                                        },
                                                        "type": {
                                                            "constant": "Sale"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "price",
                                                    "type": "object",
                                                    "source": "AIMIA_price_data",
                                                    "selector": "/root/root/discount",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Save Price"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "type": {
                                                            "constant": "Save"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                }
                                            ]
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object_array",
                                    "selector": "properties[./name='gender']/value",
                                    "object": {
                                        "gender": {
                                            "type": "string",
                                            "selector": "."
                                        }
                                    }
                                },
                                "reviewrating": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "rating": {
                                            "type": "string",
                                            "selector": "properties[./name='aggregated_rating']/value"
                                        },
                                        "reviewcount": {
                                            "type": "string",
                                            "selector": "properties[./name='reviewcount']/value|properties[./name='review_count']/value"
                                        },
                                        "ratings": {
                                            "type": "object_array",
                                            "selector": "properties[./name='max_rating']/value|properties[./name='aggregated_rating']/value|properties[./name='avg_rating']/value",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "./../name"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                },
                                "cartinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "expecteddelivery": {
                                            "type": "string",
                                            "selector": "properties[./name[text()='deliverydaysmin']]/value"
                                        },
                                        "expecteddeliverydays": {
                                            "type": "string",
                                            "selector": "properties[./name[text()='deliverydaysmax']]/value"
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "skus",
                        "source": "AIMIA_product_data",
                        "selector": "/root/product/skus",
                        "childDataName": "AIMIA_product_child_data",
                        "processParallel": true,
                        "params": {
                            "skuid": {
                                "type": "string",
                                "selector": "skuId|skuid",
                                "skipFromMustCheck": true,
                                "paramNotRequired": "true"
                            }
                        },
                        "typeSelector": {
                            "defaultType": "AIMIA_PRODUCT_CHILD"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "AIMIA_PRODUCT_CHILD": {
                "sources": {
                    "AIMIA_childprice_data": {
                        "type": "forcedjsonarray",
                        "url": {
                            "url": {
                                "constant": "http://api.intelligencenode.com/real_time_price"
                            },
                            "method": "POST",
                            "contentType": "application/json",
                            "content": "{\"ids\":[\"{@skuid}\"],\"user\":\"{@pricecall_user}\",\"pass\":\"{@pricecall_pass}\"}"
                        },
                        "allowedResponseCodes": [
                            400
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "product"
                        },
                        "name": {
                            "type": "string",
                            "source": "AIMIA_product_child_data",
                            "selector": "name"
                        },
                        "image": {
                            "type": "string",
                            "source": "AIMIA_product_child_data",
                            "selector": "properties[./name='image']/value"
                        },
                        "link": {
                            "type": "string",
                            "source": "AIMIA_product_child_data",
                            "selector": "properties[./name='skuurl']/value",
                            "extractor": "(.*)\\?(.*)$",
                            "formatter": "{@0}"
                        },
                        "identifier": {
                            "type": "string",
                            "source": "AIMIA_product_child_data",
                            "multiSelector": [
                                "skuId|skuid",
                                "productId|productid"
                            ]
                        },
                        "properties": {
                            "type": "object",
                            "source": "AIMIA_product_child_data",
                            "selector": ".",
                            "params": {
                                "regprice": {
                                    "type": "string",
                                    "source": "AIMIA_product_child_data",
                                    "selector": "./properties[./name='mrp']/value",
                                    "skipFromMustCheck": true
                                },
                                "saleprice": {
                                    "type": "string",
                                    "source": "AIMIA_product_child_data",
                                    "selector": "./properties[./name='price']/value",
                                    "skipFromMustCheck": true
                                }
                            },
                            "object": {
                                "skuprops": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "color": {
                                            "type": "object_array",
                                            "selector": "./properties[./name='color']/value|./properties[./name='colors']/value",
                                            "object": {
                                                "name": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "size1": {
                                            "type": "object_array",
                                            "selector": "./properties[./name='size1']/value",
                                            "object": {
                                                "name": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                },
                                "iteminfo": {
                                    "type": "object",
                                    "source": "AIMIA_product_child_data",
                                    "selector": ".",
                                    "object": {
                                         "additionalinfo": {
                                            "type": "object_array",
                                            "selector": "properties[./name[text()='sellerName']]/value",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "./../name"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "bigimage": {
                                            "type": "object_array",
                                            "selector": "properties[./name='zoom']/value",
                                            "object": {
                                                "image": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "thumbimage": {
                                            "type": "object_array",
                                            "selector": "properties[./name='thumb']/value",
                                            "object": {
                                                "image": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },                                        
                                        "categoryids": {
                                            "type": "object_array",
                                            "selector": "defaultParentCategoryId|defaultParentCategoryid",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "local-name()"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": ".",
                                                    "extractor":".*\\|([^|]*)\\|top$",
                                                    "formatter":"{@0}",
                                                    "formatterOnNull":"{@0}"
                                                }
                                            }
                                        },
                                        "brand": {
                                            "type": "string",
                                            "selector": "properties[./name[text()='brand']]/value"
                                        },
                                        "iscollection": {
                                            "type": "string",
                                            "selector": "collection"
                                        },
                                        "additionalimages":{
                                            "type": "string",
                                            "selector": "properties[./name[text()='altimages']]/value",
                                            "txtToJson": "true"
                                        }
                                    }
                                },
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                       "displayorder": {
                                            "type": "string",
                                            "selector": "properties[./name[text()='rank']]/value"
                                        }
                                    }
                                },
                                "buyinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "isnew": {
                                            "type": "string",
                                            "selector": "properties[./name[text()='new']]/value"
                                        },
                                        "instock": {
                                            "type": "string",
                                            "selector": "active"
                                        },
                                        "pricing": {
                                            "type": "object",
                                            "selector": ".",
                                            "params": {
                                                "regprice": {
                                                    "type": "string",
                                                    "source": "AIMIA_childprice_data",
                                                    "selector": "/root/root/mrp",
                                                    "skipFromMustCheck": true,
                                                    "paramNotRequired": "true",
                                                    "emptyCheck": "true"
                                                },
                                                "saleprice": {
                                                    "type": "string",
                                                    "selector": "/root/root/price",
                                                    "source": "AIMIA_childprice_data",
                                                    "skipFromMustCheck": true,
                                                    "paramNotRequired": "true",
                                                    "emptyCheck": "true"
                                                }
                                            },
                                            "serialObject": [
                                                {
                                                    "name": "price",
                                                    "source": "AIMIA_childprice_data",
                                                    "selector": ".[{@regprice}!='null']",
                                                    "type": "object",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Reg Price"
                                                        },
                                                        "value": {
                                                            "constant": "{@regprice}"
                                                        },
                                                        "type": {
                                                            "constant": "Reg"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "price",
                                                    "source": "AIMIA_childprice_data",
                                                    "selector": ".[{@saleprice}!='null']",
                                                    "type": "object",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Sale Price"
                                                        },
                                                        "value": {
                                                            "constant": "{@saleprice}"
                                                        },
                                                        "type": {
                                                            "constant": "Sale"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "price",
                                                    "type": "object",
                                                    "selector": "/root/root/discount",
                                                    "source": "AIMIA_childprice_data",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Save Price"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "type": {
                                                            "constant": "Save"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                }
                                            ]
                                        },
                                        "rewards":{
                                            "type":"object_array",
                                            "selector":".",
                                            "object":{
                                                "points":{
                                                    "type":"string",
                                                    "selector":"properties[./name='loyaltyPoints']/value"
                                                }
                                            }   
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object_array",
                                    "selector": "properties[./name[text()='gender']]/value",
                                    "object": {
                                        "gender": {
                                            "type": "string",
                                            "selector": "."
                                        }
                                    }
                                },
                                "cartinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "expecteddelivery": {
                                            "type": "string",
                                            "selector": "properties[./name[text()='deliverydaysmin']]/value"
                                        },
                                        "expecteddeliverydays": {
                                            "type": "string",
                                            "selector": "properties[./name[text()='deliverydaysmax']]/value"
                                        },
                                        "discounttotal":{
                                            "type": "string",
                                            "selector": "properties[./name[text()='discount_percent']]/value"
                                        }
                                    }
                                },
                                "reviewrating": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "rating": {
                                            "type": "string",
                                            "selector": "properties[./name='aggregated_rating']/value"
                                        },
                                        "reviewcount": {
                                            "type": "string",
                                            "selector": "properties[./name='reviewcount']/value|properties[./name='review_count']/value"
                                        },
                                        "ratings": {
                                            "type": "object_array",
                                            "selector": "properties[./name='max_rating']/value|properties[./name='aggregated_rating']/value",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "./../name"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_PRODUCT_SKU": {
                "streamType": "true",
                "streamGroup": "core",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "product",
                "idPattern": ".*",
                "urlPattern": "sku",
                "overrideCacheControl": "900",
                "cacheKey": [
                    {
                        "constant": "AIMIA_PRODUCT_SKU_{@pimcampaignid}_{@id}"
                    },
                    {
                        "constant": "timestamp_{@timestamp}",
                        "paramNotRequired": "true"
                    }
                ],
                "cacheInternalMapKey": [
                    {
                        "constant": "data"
                    }
                ],
                "mustHaveCacheList": [
                    "children"
                ],
                "sources": {
                    "AIMIA_product_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "http://{@host_name_aimiacorecalls}/pim/sku?campaignId={@pimcampaignid}&skuId={@id}"
                            },
                            "params": {
                                "locale": [
                                    {
                                        "constant": "{@locale}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "previewTime": [
                                    {
                                        "constant": "{@timestamp}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ]
                            },
                            "additionalParses":[
                                {
                                    "type":"jsonarray",
                                    "name":"aimia_images_data",
                                    "startStr":"\"properties\":{\"altimages\":\"",
                                    "endStr":"]",
                                    "includeStart": false,
                                    "includeEnd": true,
                                    "doUnescapeJavaString": "true"
                            }
                            ]
                        },
                        "fixJsonKeysForXML": true
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "product"
                        },
                        "responseCode": {
                            "type": "string",
                            "source": "AIMIA_product_data",
                            "selector": "//root/responseCode"
                        },
                        "responseMessage": {
                            "type": "string",
                            "source": "AIMIA_product_data",
                            "selector": "//root/responseMessage"
                        },
                        "name": {
                            "type": "string",
                            "source": "AIMIA_product_data",
                            "selector": "/root/product/name"
                        },
                        "image": {
                            "type": "string",
                            "source": "AIMIA_product_data",
                            "selector": "/root/product/properties[./name='image']/value"
                        },
                        "link": {
                            "type": "string",
                            "source": "AIMIA_product_data",
                            "selector": "/root/product/skus/properties[./name='skuurl']/value"
                        },
                        "identifier": {
                            "type": "string",
                            "source": "AIMIA_product_data",
                            "multiSelector": [
                                "/root/product/skuId|/root/product/skuid",
                                "/root/product/productId|/root/product/productid"
                            ]
                        },
                        "properties": {
                            "type": "object",
                            "selector": "/root/product",
                            "source": "AIMIA_product_data",
                            "params": {
                                "orgregprice": {
                                    "type": "string",
                                    "selector": "./properties[./name='mrp']/value",
                                    "skipFromMustCheck": true
                                },
                                "orgsaleprice": {
                                    "type": "string",
                                    "selector": "./properties[./name='price']/value",
                                    "skipFromMustCheck": true
                                }
                            },
                            "object": {
                                "skuprops": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "color": {
                                            "type": "object_array",
                                            "selector": "properties[./name='color']/value",
                                            "object": {
                                                "name": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "size": {
                                            "type": "object_array",
                                            "selector": "properties[./name='size']/value",
                                            "object": {
                                                "name": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                },
                                "iteminfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "categoryids": {
                                            "type": "object_array",
                                            "source": "AIMIA_product_data",
                                            "selector": "/root/product/defaultParentCategoryId|/root/product/defaultParentCategoryid",
                                            "object": {
                                                "label": {
                                                    "constant": "parent category"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": ".",
                                                    "extractor":".*\\|([^|]*)\\|top$",
                                                    "formatter":"{@0}",
                                                    "formatterOnNull":"{@0}"
                                                }
                                            }
                                        },
                                        "brand": {
                                            "type": "string",
                                            "selector": "properties[./name='brand']/value"
                                        },
                                        "iscollection": {
                                            "type": "string",
                                            "selector": "collection"
                                        },
                                        "bigimage": {
                                            "type": "object_array",
                                            "source": "AIMIA_product_data",
                                            "selector": "/root/product/properties[./name='zoom']/value",
                                            "object": {
                                                "image": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "thumbimage": {
                                            "type": "object_array",
                                            "source": "AIMIA_product_data",
                                            "selector": "/root/product/properties[./name='thumb']/value",
                                            "object": {
                                                "image": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "description": {
                                            "type": "object_array",
                                            "selector": "properties[./name='description']/value",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "info"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        },
                                        "additionalimages": {
                                            "type": "object_array",
                                            "source":"aimia_images_data",
                                            "selector":"//root/root/image|//root/root/thumb",
                                            "object":{
                                                "image":{
                                                    "type":"string",
                                                    "selector":"./image"
                                                },
                                                "value":{
                                                    "type":"string",
                                                    "selector":"./image",
                                                    "extractor":".*\\-(.*)\\.jpg",
                                                    "formatter":"{@0}",
                                                    "formatterOnNull":"{@0}"
                                                }
                                            }
                                        }
                                    },
                                    "serialObject": [
                                        {
                                            "name": "parent",
                                            "type": "object",
                                            "source": "AIMIA_product_data",
                                            "selector": "/root/product/defaultParentCategoryId|/root/product/defaultParentCategoryid",
                                            "object": {
                                                "label": {
                                                    "constant": "parentcategoryid"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            },
                                            "addToArray": "categoryids"
                                        }
                                    ]
                                },
                                "buyinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "sources": {
                                        "AIMIA_price_data": {
                                            "type": "forcedjsonarray",
                                            "url": {
                                                "url": {
                                                    "constant": "http://api.intelligencenode.com/real_time_price"
                                                },
                                                "method": "POST",
                                                "contentType": "application/json",
                                                "content": "{\"ids\":[\"{@id}\"],\"user\":\"{@pricecall_user}\",\"pass\":\"{@pricecall_pass}\"}"
                                            },
                                            "allowedResponseCodes": [
                                                400
                                            ]
                                        }
                                    },
                                    "object": {
                                        "isnew":{
                                            "type": "string",
                                            "selector": "/root/product/properties[./name='new']/value"
                                        },
                                        "pricing": {
                                            "type": "object",
                                            "selector": ".",
                                            "params": {
                                                "orgregprice": {
                                                    "type": "string",
                                                    "selector": "/root/root/mrp",
                                                    "source": "AIMIA_price_data",
                                                    "skipFromMustCheck": true,
                                                    "paramNotRequired": "true",
                                                    "emptyCheck": "true"
                                                },
                                                "orgsaleprice": {
                                                    "type": "string",
                                                    "selector": "/root/root/price",
                                                    "source": "AIMIA_price_data",
                                                    "skipFromMustCheck": true,
                                                    "paramNotRequired": "true",
                                                    "emptyCheck": "true"
                                                }
                                            },
                                            "serialObject": [
                                                {
                                                    "name": "price",
                                                    "source": "AIMIA_price_data",
                                                    "selector": ".[{@orgregprice}!='null']",
                                                    "type": "object",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Reg Price"
                                                        },
                                                        "value": {
                                                            "constant": "{@orgregprice}"
                                                        },
                                                        "type": {
                                                            "constant": "Reg"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "price",
                                                    "source": "AIMIA_price_data",
                                                    "selector": ".[{@orgsaleprice}!='null']",
                                                    "type": "object",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Sale Price"
                                                        },
                                                        "value": {
                                                            "constant": "{@orgsaleprice}"
                                                        },
                                                        "type": {
                                                            "constant": "Sale"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "price",
                                                    "type": "object",
                                                    "source": "AIMIA_price_data",
                                                    "selector": "/root/root/discount",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Save Price"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "type": {
                                                            "constant": "Save"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                }
                                            ]
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object_array",
                                    "selector": "properties[./name='gender']/value",
                                    "object": {
                                        "gender": {
                                            "type": "string",
                                            "selector": "."
                                        }
                                    }
                                },
                                "reviewrating": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "rating": {
                                            "type": "string",
                                            "selector": "properties[./name='average_rating']/value"
                                        },
                                        "reviewcount": {
                                            "type": "string",
                                            "selector": "properties[./name='reviewcount']/value"
                                        },
                                        "ratings": {
                                            "type": "object_array",
                                            "selector": "properties[./name='max_rating']/value|properties[./name='aggregated_rating']/value",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "./../name"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "skus",
                        "source": "AIMIA_product_data",
                        "selector": "/root/product/skus",
                        "childDataName": "AIMIA_product_child_data",
                        "processParallel": true,
                        "params": {
                            "skuid": {
                                "type": "string",
                                "selector": "skuId|skuid",
                                "skipFromMustCheck": true,
                                "paramNotRequired": "true"
                            }
                        },
                        "typeSelector": {
                            "defaultType": "AIMIA_PRODUCT_CHILD"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            
            "AIMIA_login": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "login",
                "idPattern": ".*",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "sources": {
                    "login_user": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/login"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ]
                            },
                            "method": "POST"
                        },
                        "allowedResponseCodes": [
                            401,
                            400,
                            500,
                            406
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "authtoken": {
                            "type": "string",
                            "source": "login_user",
                            "selector": "//root/access_token"
                        },
                        "authsecuretoken": {
                            "type": "string",
                            "source": "login_user",
                            "selector": "//root/refresh_token"
                        },
                        "type": {
                            "constant": "login"
                        },
                        "properties": {
                            "type": "object",
                            "source": "login_user",
                            "selector": "//root",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./errors/validationCode"
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": "./access_token",
                                            "extractor": "(.*)",
                                            "isnull": "Failure",
                                            "isnotnull": "Success"
                                        },
                                        "expirytime": {
                                            "type": "string",
                                            "selector": "./expires_in"
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "userid": {
                                            "type": "string",
                                            "selector": "./memberId"
                                        },
                                        "token": {
                                            "type": "string",
                                            "selector": "./refresh_token"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_RefreshToken": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "refreshtoken",
                "idPattern": "null",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "sources": {
                    "refreshtoken_user": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/refresh_token?refresh_token={@refresh_token}"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            401,
                            400,
                            406,
                            500
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "authtoken": {
                            "type": "string",
                            "source": "refreshtoken_user",
                            "selector": "//access_token"
                        },
                        "type": {
                            "constant": "refreshtoken"
                        },
                        "properties": {
                            "type": "object",
                            "source": "refreshtoken_user",
                            "selector": "//root",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./error_description"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./error"
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": "./access_token",
                                            "extractor": "(.*)",
                                            "isnull": "Failure",
                                            "isnotnull": "Success"
                                        },
                                        "expirytime": {
                                            "type": "string",
                                            "selector": "./expires_in"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_chechoutAuth": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "checkoutauth",
                "idPattern": "null",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "sources": {
                    "checkoutauth_user": {
                        "type": "json",
                        "timeoutMsecs": 7000,
                        "url": {
                            "url": {
                                "constant": "https://{@host_name_checkout}/oauth/token?grant_type=client_credentials"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/x-www-form-urlencoded"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@key}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            401,
                            400,
                            406,
                            500
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "checkoutauth"
                        },
                        "properties": {
                            "type": "object",
                            "source": "checkoutauth_user",
                            "selector": "//root",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "errormessage": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./error_description",
                                                "./errors/validationMessage"
                                            ]
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./error",
                                                "./errors/validationCode"
                                            ]
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": "./access_token",
                                            "extractor": "(.*)",
                                            "isnull": "Failure",
                                            "isnotnull": "Success"
                                        },
                                        "expirytime": {
                                            "type": "string",
                                            "selector": "./expires_in"
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "token": {
                                            "type": "string",
                                            "selector": "./access_token"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_Viewprofile": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "profile",
                "idPattern": "get",
                "urlPattern": "null",
                "sources": {
                    "viewprofile_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberid}/profile"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ]
                            },
                            "params": {
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,
                            406,
                            403,
                            500
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "viewprofile"
                        },
                        "properties": {
                            "type": "object",
                            "source": "viewprofile_data",
                            "selector": ".",
                            "object": {
                                "userinfo": {
                                    "type": "object_array",
                                    "source": "viewprofile_data",
                                    "selector": "//root[./firstName[string-length() > 0]]",
                                    "object": {
                                        "email": {
                                            "type": "string",
                                            "selector": "./addresses/emailAddress/value"
                                        },
                                        "firstname": {
                                            "type": "string",
                                            "selector": "./firstName[string-length() > 0]"
                                        },
                                        "lastname": {
                                            "type": "string",
                                            "selector": "./lastName[string-length() > 0]"
                                        },
                                        "gender": {
                                            "type": "string",
                                            "selector": "./gender[string-length() > 0]"
                                        },
                                        "dob": {
                                            "type": "string",
                                            "selector": "./dateOfBirth[string-length() > 0]"
                                        },
                                        "salutation": {
                                            "type": "string",
                                            "selector": "./salutation[string-length() > 0]"
                                        },
                                        "address1": {
                                            "type": "string",
                                            "selector": "./addresses/physicalAddress/Address_Line1[string-length() > 0]"
                                        },
                                        "address2": {
                                            "type": "string",
                                            "selector": "./addresses/physicalAddress/Address_Line2[string-length() > 0]"
                                        },
                                        "address3": {
                                            "type": "string",
                                            "selector": "./addresses/physicalAddress/Address_Line3[string-length() > 0]"
                                        },
                                        "addresstype": {
                                            "type": "string",
                                            "selector": "./addresses/physicalAddress/addressChannelName[string-length() > 0]"
                                        },
                                        "city": {
                                            "type": "string",
                                            "selector": "./addresses/physicalAddress/City[string-length() > 0]"
                                        },
                                        "country": {
                                            "type": "string",
                                            "selector": "./addresses/physicalAddress/Country[string-length() > 0]"
                                        },
                                        "postalcode": {
                                            "type": "string",
                                            "selector": "./addresses/physicalAddress/Post_Code[string-length() > 0]"
                                        },
                                        "phonenumber": {
                                            "type": "string",
                                            "selector": "./addresses/mobile/value"
                                        },
                                        "county": {
                                            "type": "string",
                                            "selector": "./addresses/physicalAddress/County[string-length() > 0]"
                                        }
                                    }
                                },
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./errors/invalidValue",
                                                "./errors/validationCode"
                                            ]
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_profile_Getpreferences": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "profile",
                "idPattern": "getpreferences",
                "urlPattern": "null",
                "sources": {
                    "getpreferences_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberid}/getPreferences"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,
                            406,
                            403,
                            500
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "getpreferences"
                        },
                        "properties": {
                            "type": "object",
                            "source": "getpreferences_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "/errors/validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./errors/validationCode"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        }
                                    }
                                },
                                "preferences": {
                                    "type": "object_array",
                                    "source": "getpreferences_data",
                                    "selector": "//root/COMMUNICATION|//root/NOTIFICATION",
                                    "object": {
                                        "identifier": {
                                            "type": "string",
                                            "selector": "preferenceTypeId"
                                        },
                                        "description": {
                                            "type": "string",
                                            "selector": "preferenceAreaType"
                                        },
                                        "type": {
                                            "type": "string",
                                            "selector": "preferenceTypeName"
                                        },
                                        "childpreferences": {
                                            "type": "object_array",
                                            "selector": "./memberPreferencePayloads",
                                            "object": {
                                                "identifier": {
                                                    "type": "string",
                                                    "selector": "preferenceId"
                                                },
                                                "description": {
                                                    "type": "string",
                                                    "selector": "preferenceName"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_profile_Updatepreferences": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "profile",
                "idPattern": "updatepreferences",
                "urlPattern": "null",
                "sources": {
                    "updatepreferences_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberid}/updatePreferences"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "POST"
                        },
                        "emptyResponse": "true",
                        "allowedResponseCodes": [
                            400,
                            406,
                            403,
                            500
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "updatepreferences"
                        },
                        "properties": {
                            "type": "object",
                            "source": "updatepreferences_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./errors/validationCode"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_update_profile": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "profile",
                "idPattern": "update",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "sources": {
                    "update_profile": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberid}/updateProfile"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "POST"
                        },
                        "emptyResponse": true,
                        "allowedResponseCodes": [
                            401,
                            400,
                            406,
                            404,
                            403
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "updateprofile"
                        },
                        "properties": {
                            "type": "object",
                            "source": "update_profile",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "errormessage": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./error_description",
                                                "./errors/validationMessage"
                                            ]
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./error",
                                                "./errors/validationCode"
                                            ]
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": "./access_token",
                                            "extractor": "(.*)",
                                            "isnull": "Failure",
                                            "isnotnull": "Success"
                                        },
                                        "expirytime": {
                                            "type": "string",
                                            "selector": "./expires_in"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_register": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "register",
                "idPattern": "null",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "sources": {
                    "register_user": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/signup"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ]
                            },
                            "method": "POST"
                        },
                        "allowedResponseCodes": [
                            401,
                            500,
                            406,
                            400
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "register"
                        },
                        "properties": {
                            "type": "object",
                            "source": "register_user",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./errors/validationCode"
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage",
                                            "extractor": "(.*)",
                                            "isnull": "Success",
                                            "isnotnull": "Failure"
                                        },
                                        "expirytime": {
                                            "type": "string",
                                            "selector": "./expires_in"
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "firstname": {
                                            "type": "string",
                                            "selector": "./firstName"
                                        },
                                        "lastname": {
                                            "type": "string",
                                            "selector": "./lastName"
                                        },
                                        "middlename": {
                                            "type": "string",
                                            "selector": "./middleName"
                                        },
                                        "userid": {
                                            "type": "string",
                                            "selector": "./memberId"
                                        },
                                        "token": {
                                            "type": "string",
                                            "selector": "./refresh_token"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_offers": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "offerlookup",
                "idPattern": "null",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "sources": {
                    "member_offers_data": {
                        "type": "forcedjsonarray",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberid}/offers"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "POST"
                        },
                        "allowedResponseCodes": [
                            401,
                            500,
                            406,
                            400,
                            403,
                            404
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "offerlookup"
                        },
                        "properties": {
                            "type": "object",
                            "source": "member_offers_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./errors/validationCode"
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": "./errors/success[contains(text(),'false')]",
                                            "extractor": "(.*)",
                                            "isnull": "Success",
                                            "isnotnull": "Failure"
                                        },
                                        "expirytime": {
                                            "type": "string",
                                            "selector": "./expires_in"
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "offers",
                        "source": "member_offers_data",
                        "childDataName": "offers_child_data",
                        "selector": "//root/root",
                        "typeSelector": {
                            "defaultType": "user_offers_child"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "user_offers_child": {
                "value": {
                    "values": {
                        "identifier": {
                            "type": "string",
                            "selector": "./instanceId"
                        },
                        "name": {
                            "type": "string",
                            "selector": "./title"
                        },
                        "image": {
                            "type": "string",
                            "selector": "./imageUrl"
                        },
                        "properties": {
                            "type": "object",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "starttime": {
                                            "type": "string",
                                            "selector": "./startTime"
                                        },
                                        "expirytime": {
                                            "type": "string",
                                            "selector": "./endTime"
                                        },
                                        "salestartdate": {
                                            "type": "string",
                                            "selector": "./startDate"
                                        },
                                        "saleenddate": {
                                            "type": "string",
                                            "selector": "./endDate"
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": "./offerStatus"
                                        }
                                    }
                                },
                                "iteminfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "description": {
                                            "type": "object_array",
                                            "selector": ".[./description]",
                                            "object": {
                                                "value": {
                                                    "type": "string",
                                                    "selector": "./description"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_ForgotPassword": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "recoverpwdbyemail",
                "idPattern": "null",
                "urlPattern": "null",
                "sources": {
                    "forgotpassword_data": {
                        "type": "forcedjsonarray",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/forgotPassword"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "PUT"
                        },
                        "allowedResponseCodes": [
                            204,
                            404,
                            400,
                            500
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "recoverpwdbyemail"
                        },
                        "properties": {
                            "type": "object",
                            "source": "forgotpassword_data",
                            "selector": "//root/errors",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./validationMessage",
                                            "extractor": "(.*)",
                                            "isnull": "Success",
                                            "isnotnull": "Failure"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./validationCode"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./validationMessage"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_changePassword": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "resetpassword",
                "idPattern": "null",
                "urlPattern": "null",
                "sources": {
                    "changepassword_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberid}/changeLoginPassword"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "PUT"
                        },
                        "emptyResponse": "true",
                        "allowedResponseCodes": [
                            400,
                            401,
                            202,
                            403,
                            500
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "resetpassword"
                        },
                        "properties": {
                            "type": "object",
                            "source": "changepassword_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./validationResults/validationCode[string-length(.) > 0]",
                                                "./errors/validationCode[string-length(.) > 0]"
                                            ],
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./validationResults/validationMessage",
                                                "./errors/validationMessage"
                                            ]
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./validationResults/validationCode",
                                                "./errors/validationCode"
                                            ]
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_GetRewardItems": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "getcoupons",
                "idPattern": "null",
                "urlPattern": "null",
                "sources": {
                    "getcoupons_data": {
                        "type": "jsonarray",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/catalogue-reward-items"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "PUT"
                        },
                        "allowedResponseCodes": [
                            400,
                            401,
                            202,
                            500,
                            403
                        ],
                        "fixJsonKeysForXML": "true",
                        "appendToJSONKey": "_"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "getcoupons"
                        },
                        "properties": {
                            "type": "object",
                            "source": "getcoupons_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object_array",
                                    "source": "getcoupons_data",
                                    "selector": "//errors",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./errors/validationCode"
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object",
                                    "source": "getcoupons_data",
                                    "selector": ".",
                                    "object": {
                                        "rewards": {
                                            "type": "object_array",
                                            "selector": "//root/_root",
                                            "object": {
                                                "name": {
                                                    "type": "string",
                                                    "selector": "./_name"
                                                },
                                                "image": {
                                                    "type": "string",
                                                    "selector": "./_imageURL"
                                                },
                                                "pointsinfo": {
                                                    "type": "object_array",
                                                    "selector": ".",
                                                    "object": {
                                                        "id": {
                                                            "type": "string",
                                                            "selector": "./_catalogueId"
                                                        },
                                                        "listitems": {
                                                            "type": "string",
                                                            "selector": "./_catalogueItemId"
                                                        },
                                                        "options": {
                                                            "type": "object_array",
                                                            "selector": "./_prices",
                                                            "object": {
                                                                "id": {
                                                                    "type": "string",
                                                                    "selector": "./_priceId"
                                                                },
                                                                "points": {
                                                                    "type": "string",
                                                                    "selector": "./_roundedvalue"
                                                                },
                                                                "currency": {
                                                                    "type": "string",
                                                                    "selector": "./_currency"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_GetRewardsDetailsView": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "getcoupons",
                "idPattern": ".*",
                "urlPattern": "null",
                "sources": {
                    "getcoupons_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/reward-catalogues/{@catalogueId}/catalogue-items/{@id}"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "PUT"
                        },
                        "allowedResponseCodes": [
                            400,
                            401,
                            202,
                            403,
                            500,
                            404
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "getcoupons"
                        },
                        "properties": {
                            "type": "object",
                            "source": "getcoupons_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//errors",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./validationMessage"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./validationCode"
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object",
                                    "source": "getcoupons_data",
                                    "selector": ".",
                                    "object": {
                                        "rewards": {
                                            "type": "object_array",
                                            "selector": "//root",
                                            "object": {
                                                "name": {
                                                    "type": "string",
                                                    "selector": "./name"
                                                },
                                                "image": {
                                                    "type": "string",
                                                    "selector": "./imageURL"
                                                },
                                                "description": {
                                                    "type": "string",
                                                    "selector": "./longDescription"
                                                },
                                                "pointsinfo": {
                                                    "type": "object_array",
                                                    "selector": ".",
                                                    "object": {
                                                        "id": {
                                                            "type": "string",
                                                            "selector": "./catalogueItemId"
                                                        },
                                                        "type": {
                                                            "type": "string",
                                                            "selector": "./deliveryChannel"
                                                        },
                                                        "options": {
                                                            "type": "object_array",
                                                            "selector": "./prices",
                                                            "object": {
                                                                "id": {
                                                                    "type": "string",
                                                                    "selector": "./priceId"
                                                                },
                                                                "points": {
                                                                    "type": "string",
                                                                    "selector": "./roundedvalue"
                                                                },
                                                                "currency": {
                                                                    "type": "string",
                                                                    "selector": "./currency"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_GetBalance_profile": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "profile",
                "idPattern": "getbalance",
                "urlPattern": "null",
                "sources": {
                    "getbalance_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberid}/getBalance"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,
                            406,
                            403,
                            500
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "getbalance"
                        },
                        "properties": {
                            "type": "object",
                            "source": "getbalance_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./errors/invalidValue",
                                                "./errors/validationCode"
                                            ]
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object",
                                    "source": "getbalance_data",
                                    "selector": ".",
                                    "object": {
                                        "rewards": {
                                            "type": "object_array",
                                            "selector": ".",
                                            "object": {
                                                "pointsinfo": {
                                                    "type": "object_array",
                                                    "selector": ".",
                                                    "object": {
                                                        "options": {
                                                            "type": "object_array",
                                                            "selector": "//currencies",
                                                            "object": {
                                                                "points": {
                                                                    "type": "string",
                                                                    "selector": "./amount"
                                                                },
                                                                "label": {
                                                                    "type": "string",
                                                                    "selector": "./currency"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_GetCountries_profile": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "profile",
                "idPattern": "getcountries",
                "urlPattern": "null",
                "sources": {
                    "getcountries_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/countries"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,
                            406,
                            403,
                            500
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "getcountries"
                        },
                        "properties": {
                            "type": "object",
                            "source": "getcountries_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./errors/invalidValue",
                                                "./errors/validationCode"
                                            ]
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object_array",
                                    "source": "getcountries_data",
                                    "selector": "//root/enabledCountries",
                                    "object": {
                                        "country": {
                                            "type": "string",
                                            "selector": "./name"
                                        },
                                        "countryformat": {
                                            "type": "string",
                                            "selector": "./code"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_GetAddresschannels_profile": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "profile",
                "idPattern": "getaddresschannels",
                "urlPattern": "null",
                "sources": {
                    "getAddressChannels_data": {
                        "type": "forcedjsonarray",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/getAddressChannels"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,
                            406,
                            403,
                            500
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "Get address channels"
                        },
                        "properties": {
                            "type": "object",
                            "source": "getAddressChannels_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//errors",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success",
                                            "xmlText": true
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./validationCode"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./validationMessage"
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object_array",
                                    "source": "getAddressChannels_data",
                                    "selector": "//root[./description]",
                                    "object": {
                                        "addresstype": {
                                            "type": "string",
                                            "selector": "./channelType"
                                        },
                                        "message": {
                                            "type": "string",
                                            "selector": "./description"
                                        },
                                        "list": {
                                            "type": "object_array",
                                            "selector": "./selectedChannelSubTypes[./name]",
                                            "object": {
                                                "name": {
                                                    "type": "string",
                                                    "selector": "./name"
                                                },
                                                "description": {
                                                    "type": "string",
                                                    "selector": "./description"
                                                },
                                                "type": {
                                                    "type": "string",
                                                    "selector": "./type"
                                                }
                                            }
                                        },
                                        "additionalinfo": {
                                            "type": "object_array",
                                            "selector": "./name|programCode",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "local-name()"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_GetEarningCodes": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "profile",
                "idPattern": "getearningcodes",
                "urlPattern": "null",
                "sources": {
                    "getearningcodes_data": {
                        "type": "forcedjsonarray",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/getEarningCodes"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,
                            401,
                            202,
                            500,
                            403
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "getearningcodes"
                        },
                        "properties": {
                            "type": "object",
                            "source": "getearningcodes_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//errors",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./validationCode"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./validationMessage"
                                        }
                                    }
                                },
                                "couponinfo": {
                                    "type": "object_array",
                                    "selector": "//root[./name[string-length()>0]]",
                                    "object": {
                                        "identifier": {
                                            "type": "string",
                                            "selector": "./id"
                                        },
                                        "name": {
                                            "type": "string",
                                            "selector": "./name"
                                        },
                                        "code": {
                                            "type": "string",
                                            "selector": "./code"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_processEarningCodes": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "profile",
                "idPattern": "processearningcodes",
                "urlPattern": "null",
                "sources": {
                    "processearningcodes_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/processEarningCode"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "POST"
                        },
                        "allowedResponseCodes": [
                            400,
                            401,
                            202,
                            500,
                            403
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "responseCode": {
                            "type": "string",
                            "source": "processearningcodes_data",
                            "multiSelector": [
                                "//errors/success",
                                "//errors/validationCode"
                            ],
                            "extractor": "false",
                            "isnull": "0",
                            "isnotnull": "1"
                        },
                        "type": {
                            "constant": "processearningcodes"
                        },
                        "properties": {
                            "type": "object",
                            "source": "processearningcodes_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "errorkey": {
                                            "type": "string",
                                            "selector": "//errors/field"
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": "//muresponse[string-length(.) > 0]"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "//errors/validationCode"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "//errors/validationMessage"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "Aimia_addtoBag": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "addtobag",
                "idPattern": "null",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "sources": {
                    "aimia_addtobag_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberId}/basket/item"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "POST"
                        },
                        "allowedResponseCodes": [
                            401,
                            400,
                            500,
                            406,
                            403
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "addtobag"
                        },
                        "properties": {
                            "type": "object",
                            "source": "aimia_addtobag_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "//errors/validationMessage[string-length(.) > 0]"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "//errors/validationCode[string-length(.) > 0]"
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": "//errors/validationCode[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object_array",
                                    "selector": "//root/deliveryAddresses/value",
                                    "object": {
                                        "city": {
                                            "type": "string",
                                            "selector": "./City[string-length() > 0]"
                                        },
                                        "firstname": {
                                            "type": "string",
                                            "selector": "./FirstName[string-length() > 0]"
                                        },
                                        "lastname": {
                                            "type": "string",
                                            "selector": "./LastName[string-length() > 0]"
                                        },
                                        "address1": {
                                            "type": "string",
                                            "selector": "./Address_Line1[string-length() > 0]"
                                        },
                                        "address2": {
                                            "type": "string",
                                            "selector": "./Address_Line2[string-length() > 0]"
                                        },
                                        "address3": {
                                            "type": "string",
                                            "selector": "./Address_Line3[string-length() > 0]"
                                        },
                                        "country": {
                                            "type": "string",
                                            "selector": "./Country[string-length() > 0]"
                                        },
                                        "countryshortform": {
                                            "type": "string",
                                            "selector": "./CountryCode[string-length() > 0]"
                                        },
                                        "zipcode": {
                                            "type": "string",
                                            "selector": "./Post_Code[string-length() > 0]"
                                        },
                                        "addresstype": {
                                            "type": "string",
                                            "selector": "./../name/value[string-length() > 0]"
                                        },
                                        "county": {
                                            "type": "string",
                                            "selector": "./County[string-length() > 0]"
                                        },
                                        "addressinfo": {
                                            "type": "object_array",
                                            "selector": "./Building_Number[string-length() > 0]",
                                            "object": {
                                                "housenumber": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                },
                                "storeinfo": {
                                    "type": "object_array",
                                    "selector": "//root/enabledCountries",
                                    "object": {
                                        "country": {
                                            "type": "string",
                                            "selector": "name[string-length() > 0]"
                                        },
                                        "countrycode": {
                                            "type": "string",
                                            "selector": "code[string-length() > 0]"
                                        },
                                        "storetype": {
                                            "type": "string",
                                            "selector": "activeAddressTemplateName[string-length() > 0]"
                                        },
                                        "links": {
                                            "type": "object_array",
                                            "selector": "links[string-length() > 0]",
                                            "object": {
                                                "link": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "products",
                        "source": "aimia_addtobag_data",
                        "childDataName": "Aimia_AddtoBag_Data_child",
                        "selector": "//root/basketItems[./catalogueItemId]",
                        "typeSelector": {
                            "defaultType": "_AimiaAddtoBagproductchild"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "_AimiaAddtoBagproductchild": {
                "value": {
                    "values": {
                        "identifier": {
                            "type": "string",
                            "selector": "./catalogueItemId[string-length() > 0]"
                        },
                        "name": {
                            "type": "string",
                            "selector": "./catalogueItemName[string-length() > 0]"
                        },
                        "properties": {
                            "type": "object",
                            "selector": ".",
                            "object": {
                                "cartinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "message": {
                                            "type": "string",
                                            "selector": "./catalogueItemDesc[string-length() > 0]"
                                        },
                                        "delivery": {
                                            "type": "string",
                                            "selector": "./deliveryChannelCode[string-length() > 0]"
                                        },
                                        "quantity": {
                                            "type": "string",
                                            "selector": "./quantity[string-length() > 0]"
                                        },
                                        "deliveryimage": {
                                            "type": "string",
                                            "selector": "./imageURL[string-length() > 0]"
                                        }
                                    }
                                },
                                "buyinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "pricing": {
                                            "type": "object",
                                            "selector": "./memberOrderItemPriceResponses/totalPrice[string-length() > 0]",
                                            "serialObject": [
                                                {
                                                    "name": "totalprice",
                                                    "selector": ".",
                                                    "type": "object",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Sale"
                                                        },
                                                        "type": {
                                                            "constant": "Sale"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "qty": {
                                                            "type": "string",
                                                            "selector": "./../../quantity[string-length() > 0]"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "loyaltyprice",
                                                    "selector": "./../loyaltyPrice[string-length() > 0]",
                                                    "type": "object",
                                                    "object": {
                                                        "label": {
                                                            "constant": "Loyalty"
                                                        },
                                                        "type": {
                                                            "type": "string",
                                                            "selector": "./../loyaltyCurrency[string-length() > 0]"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "."
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                }
                                            ]
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object_array",
                                    "selector": "./address[./value/FirstName[string-length() > 0]]|./address[./FirstName[string-length() > 0]]",
                                    "object": {
                                        "city": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./City[string-length() > 0]",
                                                "./value/City[string-length() > 0]"
                                            ]
                                        },
                                        "firstname": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./FirstName[string-length() > 0]",
                                                "./value/FirstName[string-length() > 0]"
                                            ]
                                        },
                                        "lastname": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./LastName[string-length() > 0]",
                                                "./value/LastName[string-length() > 0]"
                                            ]
                                        },
                                        "address1": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./Address_Line1[string-length() > 0]",
                                                "./value/Address_Line1[string-length() > 0]"
                                            ]
                                        },
                                        "address2": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./Address_Line2[string-length() > 0]",
                                                "./value/Address_Line2[string-length() > 0]"
                                            ]
                                        },
                                        "address3": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./Address_Line3[string-length() > 0]",
                                                "./value/Address_Line3[string-length() > 0]"
                                            ]
                                        },
                                        "country": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./Country[string-length() > 0]",
                                                "./value/Country[string-length() > 0]"
                                            ]
                                        },
                                        "countryshortform": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./CountryCode[string-length() > 0]",
                                                "./value/CountryCode[string-length() > 0]"
                                            ]
                                        },
                                        "zipcode": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./Post_Code[string-length() > 0]",
                                                "./value/Post_Code[string-length() > 0]"
                                            ]
                                        },
                                        "addresstype": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./channelType[string-length() > 0]",
                                                "./name/value/channelType[string-length() > 0]"
                                            ]
                                        },
                                        "county": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./County[string-length() > 0]",
                                                "./value/County[string-length() > 0]"
                                            ]
                                        },
                                        "type": {
                                            "type": "string",
                                            "multiSelector": [
                                                "./addressTemplateName[string-length() > 0]",
                                                "./value/addressTemplateName[string-length() > 0]"
                                            ]
                                        },
                                        "additionalinfo": {
                                            "type": "object_array",
                                            "selector": ".[./value/isPrimary[string-length() > 0]]",
                                            "object": {
                                                "label": {
                                                    "constant": "isPrimary"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "multiSelector": [
                                                        "./isPrimary[string-length() > 0]",
                                                        "./value/isPrimary[string-length() > 0]"
                                                    ]
                                                }
                                            }
                                        },
                                        "addressinfo": {
                                            "type": "object_array",
                                            "selector": ".[./Building_Name[string-length() > 0]|./value/Building_Name[string-length() > 0]|./Building_Number[string-length() > 0]|./value/Building_Number[string-length() > 0]]",
                                            "object": {
                                                "organizationname": {
                                                    "type": "string",
                                                    "multiSelector": [
                                                        "./Building_Name[string-length() > 0]",
                                                        "./value/Building_Name[string-length() > 0]"
                                                    ]
                                                },
                                                "housenumber": {
                                                    "type": "string",
                                                    "multiSelector": [
                                                        "./Building_Number[string-length() > 0]",
                                                        "./value/Building_Number[string-length() > 0]"
                                                    ]
                                                }
                                            }
                                        }
                                    }
                                },
                                "orderinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "orderid": {
                                            "type": "string",
                                            "selector": "./memberOrderItemId[string-length() > 0]"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "Aimia_viewBag": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "viewbag",
                "idPattern": "null",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "sources": {
                    "aimia_viewbag_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberId}/basket"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            401,
                            400,
                            403,
                            500,
                            406
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "viewbag"
                        },
                        "properties": {
                            "type": "object",
                            "source": "aimia_viewbag_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "//errors/validationMessage[string-length(.) > 0]"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "//errors/validationCode[string-length(.) > 0]"
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": "//errors/validationCode[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object_array",
                                    "selector": "//root/deliveryAddresses/value",
                                    "object": {
                                        "city": {
                                            "type": "string",
                                            "selector": "./City[string-length() > 0]"
                                        },
                                        "firstname": {
                                            "type": "string",
                                            "selector": "./FirstName[string-length() > 0]"
                                        },
                                        "lastname": {
                                            "type": "string",
                                            "selector": "./LastName[string-length() > 0]"
                                        },
                                        "address1": {
                                            "type": "string",
                                            "selector": "./Address_Line1[string-length() > 0]"
                                        },
                                        "address2": {
                                            "type": "string",
                                            "selector": "./Address_Line2[string-length() > 0]"
                                        },
                                        "address3": {
                                            "type": "string",
                                            "selector": "./Address_Line3[string-length() > 0]"
                                        },
                                        "country": {
                                            "type": "string",
                                            "selector": "./Country[string-length() > 0]"
                                        },
                                        "countryshortform": {
                                            "type": "string",
                                            "selector": "./CountryCode[string-length() > 0]"
                                        },
                                        "zipcode": {
                                            "type": "string",
                                            "selector": "./Post_Code[string-length() > 0]"
                                        },
                                        "addresstype": {
                                            "type": "string",
                                            "selector": "./../name/value[string-length() > 0]"
                                        },
                                        "county": {
                                            "type": "string",
                                            "selector": "./County[string-length() > 0]"
                                        },
                                        "addressinfo": {
                                            "type": "object_array",
                                            "selector": "./Building_Number[string-length() > 0]",
                                            "object": {
                                                "housenumber": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                },
                                "storeinfo": {
                                    "type": "object_array",
                                    "selector": "//root/enabledCountries",
                                    "object": {
                                        "country": {
                                            "type": "string",
                                            "selector": "name[string-length() > 0]"
                                        },
                                        "countrycode": {
                                            "type": "string",
                                            "selector": "code[string-length() > 0]"
                                        },
                                        "storetype": {
                                            "type": "string",
                                            "selector": "activeAddressTemplateName[string-length() > 0]"
                                        },
                                        "links": {
                                            "type": "object_array",
                                            "selector": "links[string-length() > 0]",
                                            "object": {
                                                "link": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "products",
                        "source": "aimia_viewbag_data",
                        "childDataName": "Aimia_ViewBag_Data_child",
                        "selector": "//root/basketItems",
                        "typeSelector": {
                            "defaultType": "_AimiaAddtoBagproductchild"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "Aimia_DeleteBag": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "deletefrombag",
                "idPattern": ".*",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "sources": {
                    "aimia_deletebag_data": {
                        "type": "forcedjsonarray",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/member/{@memberid}/basket/item/{@id}"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "DELETE"
                        },
                        "allowedResponseCodes": [
                            401,
                            400,
                            403,
                            500,
                            406
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "deletebag"
                        },
                        "properties": {
                            "type": "object",
                            "source": "aimia_deletebag_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "//errors/validationMessage[string-length(.) > 0]"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "//errors/validationCode[string-length(.) > 0]"
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": "//errors/validationCode[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "products",
                        "source": "aimia_deletebag_data",
                        "childDataName": "Aimia_DeleteBag_Data_child",
                        "selector": "//root/root[./catalogueItemId]",
                        "typeSelector": {
                            "defaultType": "_AimiaAddtoBagproductchild"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "AIMIA_GetAddress_template": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "metadata",
                "idPattern": "addresstemplate",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "sources": {
                    "getadddressTemplate_Data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/address-templates/DEFAULT"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,
                            406,
                            403,
                            500,
                            404
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "address template"
                        },
                        "properties": {
                            "type": "object",
                            "source": "getadddressTemplate_Data",
                            "selector": ".",
                            "object": {
                                "userinfo": {
                                    "type": "object_array",
                                    "source": "getadddressTemplate_Data",
                                    "selector": "//fieldMetadatas[./endeavourStandardFieldName[string-length() > 0]]",
                                    "object": {
                                        "addressid": {
                                            "type": "string",
                                            "selector": "./endeavourStandardFieldName"
                                        },
                                        "additionalinfo": {
                                            "type": "object_array",
                                            "selector": "./fieldIndex|lengthOfField|exampleKey|labelKey|identifiable|searchable",
                                            "object": {
                                                "label": {
                                                    "type": "string",
                                                    "selector": "local-name()"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector": ".[string-length() > 0]"
                                                }
                                            }
                                        }
                                    }
                                },
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./errors/invalidValue"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_AddAddress": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "setbillingaddress",
                "idPattern": "null",
                "urlPattern": "null",
                "sources": {
                    "setbillingaddress_Data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberid}/delivery-addresses/{@memberOrderItemId}"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "POST"
                        },
                        "emptyResponse": "true",
                        "allowedResponseCodes": [
                            400,
                            406,
                            403,
                            500,
                            404
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "setbillingaddress"
                        },
                        "properties": {
                            "type": "object",
                            "source": "setbillingaddress_Data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./errors/validationCode"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_AddDeliveryAddress_toBasket": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "profile",
                "idPattern": "updateaddress",
                "urlPattern": "null",
                "sources": {
                    "updateaddress_Data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberid}/item/addresses/{@memberOrderItemId}"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "POST"
                        },
                        "emptyResponse": "true",
                        "allowedResponseCodes": [
                            400,
                            406,
                            403,
                            500,
                            200,
                            404
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "updateaddress"
                        },
                        "properties": {
                            "type": "object",
                            "source": "updateaddress_Data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./errors/validationCode"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_ProceedtoCheckout": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "orderstatus",
                "idPattern": "null",
                "urlPattern": "null",
                "sources": {
                    "proceedtocheckout_data": {
                        "type": "forcedjsonarray",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberid}/checkout"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,
                            401,
                            202,
                            500,
                            403
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "proceedtocheckout"
                        },
                        "properties": {
                            "type": "object",
                            "source": "proceedtocheckout_data",
                            "selector": "//root",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./errors/success[string-length(.) > 0]|./errors/validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./errors/validationCode"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        }
                                    }
                                },
                                "couponinfo": {
                                    "type": "object",
                                    "selector": "//root[./name[string-length()>0]]",
                                    "object": {
                                        "identifier": {
                                            "type": "string",
                                            "selector": "./id"
                                        },
                                        "name": {
                                            "type": "string",
                                            "selector": "./name"
                                        },
                                        "code": {
                                            "type": "string",
                                            "selector": "./code"
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "products",
                        "source": "proceedtocheckout_data",
                        "selector": "//root/root[./memberOrderItemId]",
                        "typeSelector": {
                            "defaultType": "_AimiaAddtoBagproductchild"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "AIMIA_SubmitOrder": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "submitorder",
                "idPattern": "null",
                "urlPattern": "null",
                "sources": {
                    "submitorder_data": {
                        "type": "forcedjsonarray",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberid}/placeOrderNew"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,
                            401,
                            202,
                            500,
                            403
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "submitorder"
                        },
                        "properties": {
                            "type": "object",
                            "source": "submitorder_data",
                            "selector": "//root",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./errors/success[string-length(.) > 0]|./errors/validationMessage[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "./errors/validationCode"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./errors/validationMessage"
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object",
                                    "source": "submitorder_data",
                                    "selector": "//root[./currencies]",
                                    "object": {
                                        "rewards": {
                                            "type": "object_array",
                                            "selector": ".",
                                            "object": {
                                                "pointsinfo": {
                                                    "type": "object_array",
                                                    "selector": ".",
                                                    "object": {
                                                        "options": {
                                                            "type": "object_array",
                                                            "selector": "./currencies",
                                                            "object": {
                                                                "points": {
                                                                    "type": "string",
                                                                    "selector": "./amount"
                                                                },
                                                                "currency": {
                                                                    "type": "string",
                                                                    "selector": "./currency"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "products",
                        "source": "submitorder_data",
                        "childDataName": "Aimia_submitorder_data_child",
                        "selector": "//root[./currencies]",
                        "typeSelector": {
                            "defaultType": "_Aimiasubmitorder_datachild"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "_Aimiasubmitorder_datachild": {
                "value": {
                    "values": {
                        "properties": {
                            "type": "object",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "./orderStatus/items",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./status[string-length() > 0]"
                                        }
                                    }
                                },
                                "orderinfo": {
                                    "type": "object_array",
                                    "selector": "./orderStatus/items",
                                    "object": {
                                        "orderid": {
                                            "type": "string",
                                            "selector": "./catalogueItemId[string-length() > 0]"
                                        }
                                    }
                                },
                                "cartinfo": {
                                    "type": "object_array",
                                    "selector": "./orderStatus/items",
                                    "object": {
                                        "quantity": {
                                            "type": "string",
                                            "selector": "./quantity[string-length() > 0]"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_GetRewardNow": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "getcoupons",
                "idPattern": "now",
                "urlPattern": "null",
                "sources": {
                    "getcoupons_data": {
                        "type": "forcedjsonarray",
                        "url": {
                            "url": {
                                "constant": "https://{@host_name}/lmc/members/{@memberid}/getItNow"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "POST"
                        },
                        "allowedResponseCodes": [
                            400,
                            401,
                            202,
                            500,
                            403,
                            406
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "getcoupons"
                        },
                        "properties": {
                            "type": "object",
                            "source": "getcoupons_data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "//errors/validationMessage[string-length(.) > 0]"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "selector": "//errors/validationCode[string-length(.) > 0]"
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": "//errors/validationCode[string-length(.) > 0]",
                                            "extractor": "(.*)",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        }
                                    }
                                },
                                "userinfo": {
                                    "type": "object_array",
                                    "selector": "//root/deliveryAddresses/value[./FirstName[string-length() > 0]]",
                                    "object": {
                                        "city": {
                                            "type": "string",
                                            "selector": "./City[string-length() > 0]"
                                        },
                                        "firstname": {
                                            "type": "string",
                                            "selector": "./FirstName[string-length() > 0]"
                                        },
                                        "lastname": {
                                            "type": "string",
                                            "selector": "./LastName[string-length() > 0]"
                                        },
                                        "address1": {
                                            "type": "string",
                                            "selector": "./Address_Line1[string-length() > 0]"
                                        },
                                        "address2": {
                                            "type": "string",
                                            "selector": "./Address_Line2[string-length() > 0]"
                                        },
                                        "address3": {
                                            "type": "string",
                                            "selector": "./Address_Line3[string-length() > 0]"
                                        },
                                        "country": {
                                            "type": "string",
                                            "selector": "./Country[string-length() > 0]"
                                        },
                                        "countryshortform": {
                                            "type": "string",
                                            "selector": "./CountryCode[string-length() > 0]"
                                        },
                                        "zipcode": {
                                            "type": "string",
                                            "selector": "./Post_Code[string-length() > 0]"
                                        },
                                        "addresstype": {
                                            "type": "string",
                                            "selector": "./../name/value[string-length() > 0]"
                                        },
                                        "county": {
                                            "type": "string",
                                            "selector": "./County[string-length() > 0]"
                                        },
                                        "addressinfo": {
                                            "type": "object_array",
                                            "selector": "./Building_Number[string-length() > 0]",
                                            "object": {
                                                "housenumber": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                },
                                "storeinfo": {
                                    "type": "object_array",
                                    "selector": "//root/enabledCountries[name[string-length() > 0]]",
                                    "object": {
                                        "country": {
                                            "type": "string",
                                            "selector": "name[string-length() > 0]"
                                        },
                                        "countrycode": {
                                            "type": "string",
                                            "selector": "code[string-length() > 0]"
                                        },
                                        "storetype": {
                                            "type": "string",
                                            "selector": "activeAddressTemplateName[string-length() > 0]"
                                        },
                                        "links": {
                                            "type": "object_array",
                                            "selector": "links[string-length() > 0]",
                                            "object": {
                                                "link": {
                                                    "type": "string",
                                                    "selector": "."
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "products",
                        "source": "getcoupons_data",
                        "childDataName": "Aimia_getcoupons_Data_child",
                        "selector": "//root/basketItems",
                        "typeSelector": {
                            "defaultType": "_AimiaAddtoBagproductchild"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "AIMIA_UpdateBag": {
                "streamType": "true",
                "streamGroup": "xact",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "updatebag",
                "idPattern": "null",
                "urlPattern": ".*",
                "sources": {
                    "updatebag_Data": {
                        "type": "html",
                        "url": {
                            "url": {
                                "constant": "{@type}",
                                "extractor": "(one)",
                                "formatter": "https://{@host_name}/lmc/members/{@memberid}/putBasket/item/{@memberOrderItemId}",
                                "formatterOnNull": "https://{@host_name}/lmc/members/{@memberid}/updateQuantityForBasket"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@authToken}"
                                    }
                                ]
                            },
                            "method": "POST"
                        },
                        "emptyResponse": "true",
                        "allowedResponseCodes": [
                            400,
                            406,
                            403,
                            500,
                            200,
                            204,
                            404
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "updatebag"
                        },
                        "properties": {
                            "type": "object",
                            "source": "updatebag_Data",
                            "selector": "//body",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "errormessage": {
                                            "type": "string",
                                            "xmlText": "true",
                                            "selector": ".",
                                            "extractor": ".*validationMessage\":\"(.*?)\".*",
                                            "formatter": "{@0}"
                                        },
                                        "errorcode": {
                                            "type": "string",
                                            "xmlText": "true",
                                            "selector": ".",
                                            "extractor": ".*validationCode\":\"(.*?)\".*",
                                            "formatter": "{@0}"
                                        },
                                        "status": {
                                            "type": "string",
                                            "selector": ".",
                                            "extractor": ".*validationCode\":\"(.*?)\".*|.*Some(.*?)Occured.*",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        }
                                    },
                                    "serialObject": [
                                        {
                                            "name": "SomeErrorHasOccurred",
                                            "type": "object",
                                            "selector": ".",
                                            "object": {
                                                "errormessage": {
                                                    "type": "string",
                                                    "xmlText": "true",
                                                    "selector": ".",
                                                    "extractor": ".*Some(.*?)Occured.*",
                                                    "formatter": "Some{@0}Occured",
                                                    "formatterOnNull": ""
                                                }
                                            },
                                            "addToArray": "errors"
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_GetPricesForSkus": {
                "streamType": "true",
                "streamGroup": "core",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "productdetails",
                "idPattern": "price",
                "urlPattern": "null",
                "sources": {
                    "productdetails_Data": {
                        "type": "forcedjsonarray",
                        "url": {
                            "url": {
                                "constant": "http://api.intelligencenode.com/real_time_price"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ]
                            },
                            "method": "POST",
                            "contentType": "application/json"
                        },
                        "emptyResponse": "true",
                        "allowedResponseCodes": [
                            400,
                            406,
                            403,
                            500,
                            200,
                            204,
                            404
                        ],
                        "fixJsonKeysForXML": "true",
                        "appendToJSONKey": "_"
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "price"
                        },
                        "properties": {
                            "type": "object",
                            "source": "productdetails_Data",
                            "selector": "//root",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "./_root",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./_success",
                                            "extractor": "false",
                                            "formatter": "Failure",
                                            "formatterOnNull": "Success"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./_error"
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children": [
                    {
                        "name": "skus",
                        "source": "productdetails_Data",
                        "selector": "//root/_root[./_id]",
                        "childDataName": "Skus_data",
                        "typeSelector": {
                            "defaultType": "_SKUs_details"
                        },
                        "addToMap": "children"
                    }
                ]
            },
            "_SKUs_details": {
                "value": {
                    "values": {
                        "identifier": {
                            "type": "string",
                            "selector": "./_id"
                        },
                        "properties": {
                            "type": "object",
                            "selector": ".",
                            "object": {
                                "buyinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "pricing": {
                                            "type": "object",
                                            "selector": ".",
                                            "serialObject": [
                                                {
                                                    "name": "price",
                                                    "type": "object",
                                                    "selector": "./_price",
                                                    "object": {
                                                        "label": {
                                                            "constant": "sale"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "."
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "buy",
                                                    "type": "object",
                                                    "selector": "./_mrp",
                                                    "object": {
                                                        "label": {
                                                            "constant": "reg"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "."
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "buy",
                                                    "type": "object",
                                                    "selector": "./_discount",
                                                    "object": {
                                                        "label": {
                                                            "constant": "offer"
                                                        },
                                                        "value": {
                                                            "type": "string",
                                                            "selector": "."
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                }
                                            ]
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_Request_Product_Update": {
                "streamType": "true",
                "streamGroup": "core",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "productdetails",
                "idPattern": "update",
                "urlPattern": "null",
                "sources": {
                    "updateproduct_Data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "http://sandbox.api.intelligencenode.com/subscribe"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ]
                            },
                            "method": "POST",
                            "contentType": "application/json"
                        },
                        "emptyResponse": "true",
                        "allowedResponseCodes": [
                            400,
                            406,
                            403,
                            500,
                            200,
                            204,
                            404
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "update"
                        },
                        "properties": {
                            "type": "object",
                            "source": "updateproduct_Data",
                            "selector": ".",
                            "object": {
                                "state": {
                                    "type": "object",
                                    "selector": "//root",
                                    "object": {
                                        "status": {
                                            "type": "string",
                                            "selector": "./success",
                                            "extractor": "true",
                                            "formatter": "Success",
                                            "formatterOnNull": "Failure"
                                        },
                                        "errormessage": {
                                            "type": "string",
                                            "selector": "./error"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_ProductReviews": {
                "streamType": "true",
                "streamGroup": "ratingreview",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "reviews",
                "idPattern": ".*",
                "urlPattern": "null",
                "overrideCacheControl": "900",
                "cacheKey": [
                    {
                        "constant": "AIMIA_ProductReviews_{@pimcampaignid}_{@id}_{@offset}_{@limit}"
                    }
                ],
                "cacheInternalMapKey": [
                    {
                        "constant": "data"
                    }
                ],
                "sources": {
                    "reviews_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "http://{@host_name_aimiacorecalls}/pim/getproductreviews?campaignId={@pimcampaignid}&productId={@id}&offset={@offset}&limit={@limit}"
                            },
                            "params": {
                                "locale": [
                                    {
                                        "constant": "{@locale}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ],
                                "previewTime": [
                                    {
                                        "constant": "{@timestamp}",
                                        "paramNotRequired": "true",
                                        "emptyCheck": "true"
                                    }
                                ]
                            }
                        },
                        "allowedResponseCodes": [
                            400,
                            404,
                            500
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type": {
                            "constant": "productreviews"
                        },
                        "identifier": {
                            "type": "string",
                            "source": "reviews_data",
                            "constant": "{@id}"
                        },
                        "responseCode": {
                            "type": "string",
                            "source": "reviews_data",
                            "selector": "//root/responseCode"
                        },
                        "responseMessage": {
                            "type": "string",
                            "source": "reviews_data",
                            "selector": "//root/responseMessage"
                        },
                        "properties": {
                            "type": "object",
                            "source": "reviews_data",
                            "selector": ".",
                            "object": {
                                "reviewrating": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "reviews": {
                                            "type": "object_array",
                                            "selector": "reviews",
                                            "object": {
                                                "userid": {
                                                    "type": "string",
                                                    "selector": "userId|userid"
                                                },
                                                "username": {
                                                    "type": "string",
                                                    "selector": "user"
                                                },
                                                "rating": {
                                                    "type": "string",
                                                    "selector": "rating"
                                                },
                                                "persontitle": {
                                                    "type": "string",
                                                    "selector": "title"
                                                },
                                                "reviewcreated": {
                                                    "type": "string",
                                                    "selector": "date|dateformat"
                                                },
                                                "comment": {
                                                    "type": "string",
                                                    "selector": "text"
                                                },
                                                "type": {
                                                    "type": "string",
                                                    "selector": "host"
                                                }
                                            }
                                        },
                                        "reviewcount": {
                                            "type": "string",
                                            "selector": "reviewsCount"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_Evoucher_GiftCardCategories":{
                "streamType":"true",
                "streamGroup":"giftcardservices",
                "version":"v5",
                "partnerPattern":"aimia",
                "typeFamily":"getgiftcardcategories",
                "idPattern":"null",
                "urlPattern":"null",
                "overrideCacheControl": "900",
                "cacheKey": [
                    {
                        "constant": "AIMIA_EGIFTCARDCATEGORY"
                    }
                ],
                "cacheInternalMapKey": [
                    {
                        "constant": "response"
                    }
                ],
                "sources": {
                    "evoucher_category": {
                        "type": "json",
                        "url": {
                            "url":{
                                   "constant":"https://{@host_name_evoucher}/rest/category"
                            },
                            "header": {
                                "Content-Type":[
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept":[
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization":[
                                    {
                                        "constant": "{@auth_header}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,500
                        ]
                    }
                },
                "value": {
                    "values": {
                        "type":{
                            "constant":"giftcardservices"
                        },
                        "name":{
                            "type":"string",
                            "source":"evoucher_category",
                            "selector":"//root/root_category/category_name"
                        },
                        "image":{
                            "type":"string",
                            "source":"evoucher_category",
                            "selector":"//root/root_category/category_image[string-length(.) > 0]"
                        },
                        "identifier":{
                            "type":"string",
                            "source":"evoucher_category",
                            "selector":"//root/root_category/category_id"
                        },
                        "link":{
                            "type":"string",
                            "source":"evoucher_category",
                            "selector":"//root/_links/self/href"
                        },
                        "navtype": {
                            "constant":"identifier"
                        }
                    }
                },
                "children":[
                    {
                        "name": "categories",
                        "source": "evoucher_category",
                        "selector": "//root/root_category/category_list/parent_category",
                        "childDataName": "evoucher_category_child_data",
                        "typeSelector": {
                            "defaultType": "Evoucher_top_category_child"
                            },
                            "addToMap": "children"
                    }
                ]
            },
            "Evoucher_top_category_child":{
                "value":{
                    "values":{
                        "type":{
                            "constant":"giftcardcategory"
                        },
                        "name":{
                            "type":"string",
                            "selector":"./category_name"
                        },
                        "image":{
                            "type":"string",
                            "selector":"./category_image[string-length(.) > 0]"
                        },
                        "identifier":{
                            "type":"string",
                            "selector":"./category_id"
                        },
                        "navtype": {
                            "constant":"identifier"
                        }
                    }
                },
                "children":[
                    {
                        "name": "categories",
                        "source": "evoucher_category_child_data",
                        "selector": "./child_category",
                        "childDataName": "Evoucher_category_child_data_sub_data",
                        "typeSelector": {
                            "defaultType": "Evoucher_category_child_data_sub"
                            },
                            "addToMap": "children"
                    }
                ]                
            },
            "Evoucher_category_child_data_sub":{
                "value":{
                    "values":{
                        "type":{
                            "constant":"giftcardcategory"
                        },
                        "name":{
                            "type":"string",
                            "selector":"./category_name|./name"
                        },
                        "image":{
                            "type":"string",
                            "selector":"./category_image[string-length(.) > 0]|./image[string-length(.) > 0]"
                        },
                        "identifier":{
                            "type":"string",
                            "selector":"./category_id|./id"
                        },
                        "link":{
                            "type":"string",
                            "selector":"./navigation_apicall"
                        },
                        "navtype": {
                            "constant":"identifier"
                        }
                    }
                }                
            },
            "AIMIA_Evoucher_GiftCardsByCategory":{
                "streamType":"true",
                "streamGroup":"giftcardservices",
                "version":"v5",
                "partnerPattern":"aimia",
                "typeFamily":"getgiftcardsbycategory",
                "idPattern":"null",
                "overrideCacheControl": "900",
                "cacheKey": [
                    {
                        "constant": "AIMIA_EGIFTCARDCATEGORY_{@categoryid}"
                    }
                ],
                "cacheInternalMapKey": [
                    {
                        "constant": "response"
                    }
                ],
                "sources": {
                    "evoucher_pdplist": {
                        "type": "json",
                        "url": {
                            "url":{
                                   "constant":"https://{@host_name_evoucher}/rest/category/{@categoryid}"
                            },
                            "header": {
                                "Content-Type":[
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept":[
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization":[
                                    {
                                        "constant": "{@auth_header}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,500
                        ]
                    },
                    "evocher_poolcall":{
                        "type": "json",
                        "url": {
                            "url":{
                                   "constant":"http://{@poolcall_hostname}/skavastream/entry/v5/skava/getSimplePoolEntries?campaignId={@poolcall_campaign}&offset=0&limit=1000&poolName=List_of_evouchers"
                            }
                        }
                    }
                },
                "responseFormatter": {
                    "className": "com.skava.transform.GiftcardResponseFormatter"
                },
                "value": {
                    "values": {
                        "type":{
                            "constant":"giftcardservices"
                        },
                        "name":{
                            "type":"string",
                            "source":"evoucher_pdplist",
                            "selector":"//root/name"
                        },
                        "identifier":{
                            "type":"string",
                            "source":"evoucher_pdplist",
                            "selector":"//root/id"
                        },
                        "link":{
                            "type":"string",
                            "source":"evoucher_pdplist",
                            "selector":"//root/_links/self/href"
                        },
                        "navtype": {
                            "constant":"identifier"
                        },
                        "properties":{
                            "type":"object",
                            "source":"evoucher_pdplist",
                            "selector":"//root",
                            "object":{
                                "state":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "additionallinks":{
                                            "type":"object_array",
                                            "selector":"./url",
                                            "object":{
                                                "link": {
                                                    "type": "string",
                                                    "selector":"."
                                                } 
                                            }
                                        },
                                        "productcount": {
                                            "type": "string",
                                            "selector":"./count"
                                        } 
                                    }
                                },
                                "iteminfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "additionalimages":{
                                            "type":"object_array",
                                            "selector":".",
                                            "object":{
                                                "image": {
                                                    "type": "string",
                                                    "selector":"./banner_image[string-length(.) > 0]"
                                                } 
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                "children":[
                    {
                        "name": "categories",
                        "source": "evoucher_pdplist",
                        "selector": "//root/_embedded/sub_category",
                        "childDataName": "evoucher_pdp_child_data",
                        "typeSelector": {
                            "defaultType": "Evoucher_category_child_data_sub"
                            },
                            "addToMap": "children"
                    },
                    {
                        "name": "products",
                        "source": "evoucher_pdplist",
                        "selector": "//root/_embedded/product",
                        "childDataName": "evoucher_pdplist_child_data",
                        "params":{
                            "poolidentifier":{
                                "type":"string",
                                "selector":"./id"
                            }
                        },
                        "typeSelector": {
                            "defaultType": "Evoucher_pdplist_child"
                            },
                            "addToMap": "children"
                    }
                ]
            },
            "Evoucher_pdplist_child":{
                "value":{
                    "values":{
                        "data":{
                            "type":"object_array",
                            "source":"evocher_poolcall",
                            "selector":"/root/entries[./properties/Prodid[text()='{@poolidentifier}']]",
                            "object":{
                                "pdpid":{
                                   "type":"string",
                                   "selector":"./properties/Denomination"
                                },
                                "pointsprice":{
                                   "type":"string",
                                   "selector":"./properties/PointsPrice"
                                }
                            }
                        },
                        "type":{
                            "constant":"giftcard"
                        },
                        "name":{
                            "type":"string",
                            "selector":"./name"
                        },
                        "image":{
                            "type":"string",
                            "selector":"./images/image[string-length(.) > 0]"
                        },
                        "identifier":{
                            "type":"string",
                            "selector":"./id"
                        },
                        "link":{
                            "type":"string",
                            "selector":"./navigation_apicall"
                        },
                        "navtype": {
                            "constant":"identifier"
                        },
                        "properties":{
                            "type":"object",
                            "source":"evoucher_pdplist_child_data",
                            "selector":".",
                            "object":{
                                "state":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "additionalinfo":{
                                            "type":"object_array",
                                            "selector":"./navigation_urlpath",
                                            "object":{
                                                "value": {
                                                    "type": "string",
                                                    "selector":"."
                                                },
                                                "label": {
                                                    "type": "string",
                                                    "selector":"local-name()"
                                                } 
                                            }
                                        }
                                    }
                                },
                                "iteminfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "brand": {
                                            "type": "string",
                                            "selector":"./brand_id"
                                        },
                                        "additionalimages":{
                                            "type":"object_array",
                                            "selector":"./images/small_image[string-length(.) > 0]|./images/mobile[string-length(.) > 0]",
                                            "object":{
                                                "image": {
                                                    "type": "string",
                                                    "selector":"."
                                                },
                                                "label": {
                                                    "type": "string",
                                                    "selector":"local-name()"
                                                } 
                                            }
                                        },
                                        "thumbimage":{
                                            "type":"object_array",
                                            "selector":"./images/thumbnail[string-length(.) > 0]",
                                            "object":{
                                                "image": {
                                                    "type": "string",
                                                    "selector":"."
                                                }
                                            }
                                        }
                                    }
                                },
                                "buyinfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "rewards":{
                                            "type":"object_array",
                                            "selector":"./custom_denominations",
                                            "object":{
                                                "name": {
                                                    "constant": "denomination"
                                                },
                                                "points":{
                                                    "type":"string",
                                                    "selector":"."
                                                }
                                            }
                                        },
                                        "pricing":{
                                            "type":"object",
                                            "selector":".",
                                            "object":{
                                            },
                                            "serialObject": [
                                                {
                                                    "name": "price1",
                                                    "type": "object",
                                                    "selector": "min_custom_price",
                                                    "object": {
                                                       "label":{
                                                            "type": "string",
                                                            "selector": "sale"
                                                        },
                                                         "value":{
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "ismin":{
                                                            "constant":"true"
                                                        },
                                                        "type": {
                                                            "constant":"Sale"
                                                }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "price2",
                                                    "type": "object",
                                                    "selector":"max_custom_price",
                                                    "object": {
                                                       "label":{
                                                            "type": "string",
                                                            "selector": "sale"
                                                        },
                                                         "value":{
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "ismax":{
                                                            "constant":"true"
                                                        },
                                                        "type": {
                                                            "constant":"Sale"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "price3",
                                                    "type": "object",
                                                    "selector": "price_type",
                                                    "object": {
                                                       "label":{
                                                            "type": "string",
                                                            "selector": "local-name()"
                                                        },
                                                         "value":{
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "type": {
                                                            "constant":"PriceInfo"
                                                }
                                                    },
                                                    "addToArray": "prices"
                                            }
                                            ]
                                        }
                                    },
                                    "serialObject":
                                    [
                                        {
                                            "name":"rewardpoints",
                                            "type": "object",
                                            "selector":".",
                                            "object":{
                                                "name":{
                                                    "constant":"Points Price"
                                                },
                                                "points":{
                                                    "constant":"5000"
                                                }
                                            },
                                            "addToArray":"rewards"
                                        }
                                    ]
                                }
                            }
                        }
                    }
                }                
            },
            "AIMIA_Evoucher_Giftcarddetails":{
                "streamType":"true",
                "streamGroup":"giftcardservices",
                "version":"v5",
                "partnerPattern":"aimia",
                "typeFamily":"getgiftcarddetails",
                "idPattern":"null",
                "urlPattern":"null",
                "sources": {
                    "evoucher_product_data": {
                        "type": "xml",
                        "url": {
                            "url":{
                                   "constant":"https://{@host_name_evoucher}/rest/product/{@productid}"
                            },
                            "header": {
                                "Content-Type":[
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept":[
                                    {
                                        "constant": "application/xml"
                                    }
                                ],
                                "Authorization":[
                                    {
                                        "constant": "{@auth_header}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,500
                        ]
                    },
					"evocher_poolcall" : {
						"type" : "json",
						"url" : {
							"url" : {
								"constant" : "http://{@poolcall_hostname}/skavastream/entry/v5/skava/getSimplePoolEntries?campaignId={@poolcall_campaign}&offset=0&limit=1000&poolName=List_of_evouchers&filter=%7B%22operations%22%3A%5B%5B%7B%22property%22%3A%22Prodid%22%2C%22value%22%3A%22{@productid}%22%2C%22operator%22%3A%22EQUALS%22%7D%5D%5D%7D"
							}
						}
					}
                },
				"responseFormatter" : {
					"className" : "com.skava.transform.GiftcardResponseFormatter"
				},
                "value": {
                    "values": {
						"data" : {
							"type" : "object_array",
							"source" : "evocher_poolcall",
							"selector" : "/root/entries",
							"object" : {
								"pdpid" : {
									"type" : "string",
									"selector" : "./properties/Denomination"
								},
								"pointsprice" : {
									"type" : "string",
									"selector" : "./properties/PointsPrice"
								}
							}
						},
                        "type":{
                            "constant":"giftcardservices"
                        },
                        "name":{
                            "type":"string",
                            "source":"evoucher_product_data",
                            "selector":"//magento_api/name"
                        },
                        "identifier":{
                            "type":"string",
                            "source":"evoucher_product_data",
                            "selector":"//magento_api/id"
                        },
                        "image":{
                            "type":"string",
                            "source":"evoucher_product_data",
                            "selector":"//magento_api/images"
                        },
                        "link":{
                            "type":"string",
                            "source":"evoucher_product_data",
                            "selector":"//magento_api/links/self/href"
                        },
                        "navtype": {
                            "constant":"identifier"
                        },
                        "properties":{
                            "type":"object",
                            "source":"evoucher_product_data",
                            "selector":"//magento_api",
                            "object":{
                                "buyinfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "rewards":{
                                            "type":"object_array",
                                            "selector":"./custom_denominations",
                                            "object":{
                                                "name": {
                                                    "constant": "denomination"
                                                },
                                                "points":{
                                                    "type": "string",
                                                    "selector":"."
                                        }
                                    }
                                },
                                        "pricing":{
                                            "type":"object",
                                            "selector":".",
                                            "object":{
                                            },
                                            "serialObject": [
                                                {
                                                    "name": "price1",
                                                    "type": "object",
                                                    "selector": "./min_custom_price",
                                                    "object": {
                                                       "label":{
                                                            "type": "string",
                                                            "selector": "sale"
                                                        },
                                                         "value":{
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "ismin":{
                                                            "constant":"true"
                                                        },
                                                        "type": {
                                                            "constant":"Sale"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "price2",
                                                    "type": "object",
                                                    "selector":"./max_custom_price",
                                                    "object": {
                                                       "label":{
                                                            "type": "string",
                                                            "selector": "sale"
                                                        },
                                                         "value":{
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "ismax":{
                                                            "constant":"true"
                                                        },
                                                        "type": {
                                                            "constant":"Sale"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                },
                                                {
                                                    "name": "price3",
                                                    "type": "object",
                                                    "selector": "price_type",
                                                    "object": {
                                                       "label":{
                                                            "type": "string",
                                                            "selector": "local-name()"
                                                        },
                                                         "value":{
                                                            "type": "string",
                                                            "selector": "."
                                                        },
                                                        "type": {
                                                            "constant":"PriceInfo"
                                                        }
                                                    },
                                                    "addToArray": "prices"
                                                }
                                            ]
                                        }
                                    }
                                },
                                "iteminfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "sku": {
                                            "type": "string",
                                            "selector":"./sku"
                                        },
                                        "itemtype": {
                                            "type": "string",
                                            "selector":"./product_type"
                                        },                                        
                                        "description":{
                                            "type":"object_array",
                                            "selector":".",
                                            "object":{
                                                "value": {
                                                    "type": "string",
                                                    "selector":"./description"
                                                } 
                                            }
                                        },
                                        "shortdescription":{
                                            "type":"object_array",
                                            "selector":".",
                                            "object":{
                                                "value": {
                                                    "type": "string",
                                                    "selector":"./short_description"
                                                } 
                                            }
                                        },
                                        "features":{
                                            "type":"object_array",
                                            "selector":"./tnc_mobile[string-length(.)>0]|./tnc_web[string-length(.)>0]|./tnc_mail[string-length(.)>0]",
                                            "object":{
                                                "label": {
                                                    "type": "string",
                                                    "selector":"local-name()"
                                                },
                                                "value": {
                                                    "type": "string",
                                                    "selector":"."
                                                }                                                
                                            }
                                        },
                                        "additionalimages":{
                                            "type":"object_array",
                                            "selector":"./themes/*/*",
                                            "object":{
                                                "image": {
                                                    "type": "string",
                                                    "selector":"."
                                                },
                                                "label": {
                                                    "type": "string",
                                                    "selector":"./../.",
                                                    "tagName":"true"
                                                } 
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "Evoucher_Create_order":{
                "streamType":"true",
                "streamGroup":"giftcardservices",
                "version":"v5",
                "partnerPattern":"aimia",
                "typeFamily":"submitgiftcardorder",
                "idPattern":"null",
                "urlPattern":"null",
                "cookieManager": "evoucherCookieManager",
                "sources": {
                    "evoucher_ceateorder_data": {
                        "type": "json",
                        "url": {
                            "url":{
                                   "constant":"https://{@host_name_evoucher}/rest/spend"
                            },
                            "header": {
                                "Content-Type":[
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept":[
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization":[
                                    {
                                        "constant": "{@auth_header}"
                                    }
                                ]
                            },
                            "method": "POST",
                            "contentType": "application/json"
                            
                        },
                        "appendToJSONKey":"_",
                        "allowedResponseCodes": [
                            400,500
                        ],
                        "contentGenerator":"com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type":{
                            "constant":"giftcardservices"
                        },
                        "link":{
                            "type":"string",
                            "source":"evoucher_ceateorder_data",
                            "selector":"//root/__links/_self/_href"
                        },
                        "properties":{
                            "type":"object",
                            "source":"evoucher_ceateorder_data",
                            "selector":"//root",
                            "object":{
                                "orderinfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "orderid":{
                                            "type":"string",
                                            "selector":"./_order_id"
                                        },
                                        "ordertext":{
                                            "type":"string",
                                            "selector":"./_message"
                                        }
                                    }
                                },
                                "state":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "status":{
                                            "type":"string",
                                            "selector":"./_status"
                                        }
                                    }
                                },
                                "giftcardinfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "cards":{
                                            "type":"object_array",
                                            "selector":"./_carddetails/*",
                                            "params":{
                                                "cardname":{
                                                    "type": "string",
                                                    "selector":"local-name()"
                                                }
                                            },
                                            "object":{
                                                "cardname":{
                                                    "constant":"{@cardname}"
                                                },
                                                "cardnumber": {
                                                    "type": "string",
                                                    "selector":"./_cardnumber"
                                                },
                                                "expirationdate": {
                                                    "type": "string",
                                                    "selector":"./_expiry_date"
                                                },
                                                "maskednumber": {
                                                    "type": "string",
                                                    "selector":"./_pin_or_url"
                                                },
                                                "netbalance": {
                                                    "type": "string",
                                                    "selector":"./_card_price"
                                                },
                                                "activationcode":{
                                                    "type": "string",
                                                    "selector":"./_activation_code"
                                                },
                                                "cardpin":{
                                                    "type": "string",
                                                    "selector":"./_cardpin"
                                                },
                                                "cardprice":{
                                                    "type": "string",
                                                    "selector":"./_card_price"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_Evoucher_GiftCardStatus":{
                "streamType":"true",
                "streamGroup":"giftcardservices",
                "version":"v5",
                "partnerPattern":"aimia",
                "typeFamily":"getgiftcardstatus",
                "idPattern":"null",
                "urlPattern":"null",
                "sources": {
                    "evoucher_orderstatus_data": {
                        "type": "json",
                        "url": {
                            "url":{
                                   "constant":"https://{@host_name_evoucher}/rest/status/{@orderid}"
                            },
                            "header": {
                                "Accept":[
                                {
                                    "constant":"application/json"
                                }
                                ],
                                "Authorization":[
                                    {
                                        "constant": "{@auth_header}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "allowedResponseCodes": [
                            400,500
                        ]
                    }
                },
                "value": {
                    "values": {
                        "responseMessage":{
                            "type":"string",
                            "source":"evoucher_ceateorder_data",
                            "selector":"//root/_success",
                            "extractor":"true",
                            "formatter":"success",
                            "formatterOnNull":"faliure"
                        },
                        "type":{
                            "constant":"getgiftcardstatus"
                        },
                        "link":{
                            "type":"string",
                            "source":"evoucher_orderstatus_data",
                            "selector":"//root/_links/self/href"
                        },
                        "properties":{
                            "type":"object",
                            "source":"evoucher_orderstatus_data",
                            "selector":"//root",
                            "object":{
                                "state":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "status":{
                                            "type":"string",
                                            "selector":"./status"
                                        },
                                        "additionalinfo":{
                                            "type":"object_array",
                                            "selector":"./process",
                                            "object":{
                                                "label":{
                                                    "type":"string",
                                                    "selector":"local-name()"
                                                },
                                                "value":{
                                                    "type":"string",
                                                    "selector":"."
                                                }
                                            }
                                        }
                                    }
                                },
                                "orderinfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "orderid":{
                                            "type":"string",
                                            "selector":"./order_id"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_Evoucher_Checkbalance":{
                "streamType":"true",
                "streamGroup":"giftcardservices",
                "version":"v5",
                "partnerPattern":"aimia",
                "typeFamily":"getgiftcardbalance",
                "idPattern":"null",
                "urlPattern":"null",
                "sources": {
                    "evoucher_checkbalance_data": {
                        "type": "json",
                        "url": {
                            "url":{
                                   "constant":"https://{@host_name_evoucher}/rest/checkbalance"
                            },
                            "header": {
                                "Content-Type":[
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept":[
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization":[
                                    {
                                        "constant": "{@auth_header}"
                                    }
                                ]
                            },
                            "method": "POST",
                            "contentType": "application/json"
                        },
                        "allowedResponseCodes": [
                            400,500
                        ],
                        "contentGenerator": "com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value": {
                    "values": {
                        "type":{
                            "constant":"giftcardservices"
                        },
                        "properties":{
                            "type":"object",
                            "source":"evoucher_checkbalance_data",
                            "selector":"//root",
                            "object":{
                                "state":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "expirytime":{
                                            "type":"string",
                                            "selector":"./data/expiry"
                                        }
                                    }
                                },
                                "giftcardinfo": {
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "cards": {
                                        "type": "object_array",
                                        "selector": "./data",
                                        "object": {
                                            "cardnumber": {
                                                    "type": "string",
                                                    "selector": "./cardnumber"
                                                },
                                            "netbalance": {
                                                    "type": "string",
                                                    "selector": "./cardbalance"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_Evoucher_resend": {
                "streamType": "true",
                "streamGroup": "giftcardservices",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "resendgiftcardorder",
                "idPattern": "null",
                "urlPattern": "null",
                "sources": {
                    "evoucher_Resend_data": {
                        "type": "json",
                        "url": {
                            "url": {
                            "constant": "https://{@host_name_evoucher}/rest/resend/{@orderid}"
                            },
                            "header": {
                                "Content-Type": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Accept": [
                                    {
                                        "constant": "application/json"
                                    }
                                ],
                                "Authorization": [
                                    {
                                        "constant": "{@auth_header}"
                                    }
                                ]
                            },
                            "method": "GET"
                        },
                        "appendToJSONKey": "_",
                        "allowedResponseCodes": [
                            400,
                            500,
                            404
                        ]
                    }
                },
                "value":{
                    "values":{
                        "type": {
                            "constant": "giftcardservices"
                        },
                        "properties":{
                            "type": "object",
                            "source": "evoucher_Resend_data",
                            "selector": "//root",
                            "object":{
                                "state":{
                                    "type": "object",
                                    "selector": ".",
                                    "object":{
                                        "status": {
                                            "type": "string",
                                            "selector": "./_status"
                                        }
                                    }
                                },
                                "orderinfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "orderid":{
                                            "type":"string",
                                            "selector":"./_order_id"
                                        }
                                    }
                                },
                                "giftcardinfo":{
                                    "type": "object",
                                    "selector": ".",
                                    "object": {
                                        "cards":{
                                            "type": "object_array",
                                            "selector": "./_carddetails/*",
                                            "object": {
                                                "cardnumber": {
                                                    "type": "string",
                                                    "selector": "./_cardnumber"
                                                },
                                                "expirationdate": {
                                                    "type": "string",
                                                    "selector": "./_expiry_date"
                                                },
                                                "maskednumber": {
                                                    "type": "string",
                                                    "selector": "./_pin_or_url"
                                                },
                                                "netbalance": {
                                                    "type": "string",
                                                    "selector": "./_card_price"
                                                },
                                                "activationcode": {
                                                    "type": "string",
                                                    "selector": "./activation_code"
                                                },
                                                "cardpin": {
                                                    "type": "string",
                                                    "selector": "./cardpin"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "AIMIA_Evoucher_CreateGiftcardOrder":{
                "streamType": "true",
                "streamGroup": "giftcardservices",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "creategiftcardorder",
                "idPattern": "null",
                "urlPattern": "null",
                 "sources":{
                    "evoucher_giftcardorder_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "http://{@localhost_name}/skavastream/custom/v5/aimia/process?process=com.skava.catalogservices.CreateGiftCardOrder&campaignId={@campaignId}"
                            },
                            "method": "POST",
                            "contentType": "application/x-www-form-urlencoded"
                        },
                        "appendToJSONKey":"_",
                        "contentGenerator":"com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value":{
                    "values":{
                        "type":{
                            "constant":"giftcardservices"
                        },
                        "responseMessage":{
                            "type":"string",
                            "selector":"/root/_responseMessage",
                            "source":"evoucher_giftcardorder_data"
                        },
                        "responseCode":{
                            "type":"int",
                            "selector":"/root/_responseCode",
                            "source":"evoucher_giftcardorder_data"
                        },
                        "properties":{
                            "type":"object",
                            "source":"evoucher_giftcardorder_data",
                            "selector":"/root[_responseCode='906']",
                            "object":{
                                "state":{
                                    "type":"object",
                                    "selector":"./_properties/_giftcardinfo",
                                    "object":{
                                        "status":{
                                            "type":"string",
                                            "selector":"./_status"
                                        }
                                    }
                                },
                                "orderinfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "orderid":{
                                            "type":"string",
                                            "selector":"./_orderId"
                                        }
                                    }
                                },
                                 "giftcardinfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "cards":{
                                            "type":"object_array",
                                            "selector":"./_properties/_giftcardinfo/_cards",
                                            "object":{
                                                "cardnumber": {
                                                    "type": "string",
                                                    "selector":"./_cardNumber"
                                                },
                                                "cardname": {
                                                    "type": "string",
                                                    "selector":"./_cardName"
                                                },
                                                "expirationdate": {
                                                    "type": "string",
                                                    "selector":"./_expiryDate"
                                                },
                                                "maskednumber": {
                                                    "type": "string",
                                                    "selector":"./_cardPin"
                                                },
                                                "netbalance": {
                                                    "type": "string",
                                                    "selector":"//_cardBalance"
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "Aimia_checkstatus":{
                "streamType": "true",
                "streamGroup": "giftcardservices",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "checkuserstatus",
                "idPattern": "null",
                "urlPattern": "null",
                "sources": {
                    "evoucher_checkstatus_data":{
                        "type":"json",
                        "url":{
                            "url":{
                                "constant":"https://{@streamstage_hostname}/loyaltyservices/loyalty/admin/v5/aimia/checkredemptionstatusforuser?userid=1"
                            },
                            "method":"POST"
                        }
                    }
                },
                "value":{
                    "values":{
                        "properties":{
                            "type":"object",
                            "source":"evoucher_checkstatus_data",
                            "selector":"//root",
                            "object":{
                                "state":{
                                    "type":"object",
                                    "selector":"./properties",
                                    "object":{
                                        "status":{
                                            "type":"string",
                                            "selector":"./loyaltyinfo/redemptionStatus"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "Aimia_getbalance":{
                "streamType": "true",
                "streamGroup": "giftcardservices",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "getbalance",
                "idPattern": "null",
                "urlPattern": "null",
                "sources":{
                    "evoucher_getbalance":{
                        "type":"json",
                        "url":{
                            "url":{
                                "constant":"https://{@streamstage_hostname}/loyaltyservices/loyalty/admin/v5/aimia/getbalancebyuserid?locale={@locale}&userId=1"
                            },
                            "method":"POST"
                        }
                    }
                },
                "value":{
                    "values":{
                        "properties":{
                            "type":"object",
                            "source":"evoucher_getbalance",
                            "selector":"//root/properties",
                            "object":{
                                "buyinfo":{
                                    "type":"object",
                                    "selector":"./loyaltyinfo",
                                    "object":{
                                       "rewards":{
                                           "type":"object_array",
                                           "selector":"./totalapprovedpoints",
                                           "object":{
                                               "points":{
                                                   "type":"string",
                                                   "selector":"."
                                               }
                                           }
                                       }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            "Aimia_getRedeemtranscation":{
                "streamType": "true",
                "streamGroup": "giftcardservices",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "redeemtransaction",
                "idPattern": "null",
                "urlPattern": "null",
                "sources":{
                    "evoucher_redeemtranscation":{
                        "type":"json",
                        "url":{
                            "url":{
                                "constant":"https://{@streamstage_hostname}/loyaltyservices/loyalty/v5/aimia/redeemtransaction?"
                            },
                            "method":"POST"
                        }
                    }
                },
                "value":{
                    "values":{
                        "identifier":{
                            "type":"string",
                            "source":"evoucher_redeemtranscation",
                            "selector":"//root/identifier"
                        }
                    }
                }
            },
            "Aimia_getRedeemCarddetails":{
                "streamType": "true",
                "streamGroup": "giftcardservices",
                "version": "v5",
                "partnerPattern": "aimia",
                "typeFamily": "getredeemedcarddetails",
                "idPattern": "null",
                "urlPattern": "null",
                "sources":{
                    "evoucher_giftcardorder_data": {
                        "type": "json",
                        "url": {
                            "url": {
                                "constant": "http://{@localhost_name}/skavastream/custom/v5/aimia/process?process=com.skava.catalogservices.GetRedeemCardDetails&campaignId={@campaignId}"
                            },
                            "method": "POST",
                            "contentType": "application/x-www-form-urlencoded"
                        },
                        "appendToJSONKey":"_",
                        "contentGenerator":"com.skava.transform.AimiaContentGenerator"
                    }
                },
                "value":{
                    "values":{
                        "type":{
                            "constant":"giftcardservices"
                        },
                        "responseCode":{
                            "type":"string",
                            "selector":"//root/_responseCode",
                            "source":"evoucher_giftcardorder_data"
                        },
                        "responseMessage":{
                            "type":"string",
                            "selector":"//root/_responseMessage",
                            "source":"evoucher_giftcardorder_data"
                        },
                        "properties":{
                            "type":"object",
                            "source":"evoucher_giftcardorder_data",
                            "selector":".",
                            "object":{
                                "orderinfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "orderid":{
                                            "type":"string",
                                            "selector":"/root/_orderId"
                                        }
                                    }
                                },
                                 "giftcardinfo":{
                                    "type":"object",
                                    "selector":".",
                                    "object":{
                                        "cards":{
                                            "type":"object_array",
                                            "selector":"/root/_properties/_giftcardinfo/_cards",
                                            "object":{
                                                "cardnumber": {
                                                    "type": "string",
                                                    "selector":"./_cardNumber"
                                                },
                                                "expirationdate": {
                                                    "type": "string",
                                                    "selector":"./_expiryDate"
                                                },
                                                "maskednumber": {
                                                    "type": "string",
                                                    "selector":"./_cardPin"
                                                },
                                                "netbalance": {
                                                    "type": "string",
                                                    "selector":"./_cardPrice"
                                            }
                                        }
                                    }
                                }
                            },
                            "state":{
                                "type":"object",
                                "selector":"/root/_properties/_giftcardinfo/_status",
                                "object":{
                                    "status":{
                                        "type":"string",
                                        "selector":"."
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
}
